package prepocessing;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The CSVFileReader class is used to load a csv file
 * @author andershh and hjab and Christian Rasmussen (based on the work of TA's, extended for preprocessing)
 *
 */
public class CSVFileReader {
	/**
	 * The read method reads in a csv file as a two dimensional string array.
	 * This method is utilizes the string.split method for splitting each line of the data file.
	 * String tokenizer bug fix provided by Martin Marcher.
	 * @param csvFile File to load
	 * @param seperationChar Character used to seperate entries
	 * @param nullValue What to insert in case of missing values
	 * @return Data file content as a 2D string array
	 * @throws IOException
	 */
	public static String[][] readDataFile(String csvFile, String seperationChar, String nullValue, boolean checkForQuoteMarkRange) throws IOException
	{
		List<String[]> lines = new ArrayList<String[]>();
		BufferedReader bufRdr = new BufferedReader(new FileReader(new File(csvFile)));
		
		// read the header
		String line = bufRdr.readLine();
		
		while ((line = bufRdr.readLine()) != null) {
			String[] arr = line.split(seperationChar); 
			
			for(int i = 0; i < arr.length; i++)
			{
				if(arr[i].equals(""))
				{
					arr[i] = nullValue;
				}				
			}
			
			if(checkForQuoteMarkRange)
			{
				arr = CheckForQuoteMarkRange(arr);
			}

			lines.add(arr);	
		}
		
		String[][] ret = new String[lines.size()][];
		bufRdr.close();
		return lines.toArray(ret);
	}
	
	/**
	 * This method checks a line of date from the data to ensure that no piece of the data
	 * has been split up due to how the questionnaire data set handles decimal values and lists of data - eg. "181,5" and "Fifa 2014, Grand theft auto V".
	 * This is a problem since we use the comma to split read-in lines of data into attributes of data,
	 * and since because of this attribute data may erroneously be split up e.g. "181,5" is split up into two attributes - 181 and 5.
	 * @param line Line of data to check.
	 * @return Line of data corrected if needed.
	 */
	private static String[] CheckForQuoteMarkRange(String[] line)
	{
		ArrayList<String> result = new ArrayList<String>();
		ArrayList<String> keeper = new ArrayList<String>();
		boolean addToKeeper = false;
		
		for(String part : line)
		{
			if(part.contains("\""))
			{
				if(addToKeeper)
				{
					addToKeeper = false;
					keeper.add(part);					
					String toAdd = keeper.toString();
					result.add(toAdd.substring(1, toAdd.length()-1));//Done to remove brackets from the string.
					keeper.clear();
				}
				else
				{
					addToKeeper = true;
					keeper.add(part);
				}
			}
			else
			{
				if(addToKeeper)
				{
					keeper.add(part);
				}
				else
				{
					result.add(part);
				}
			}
		}
		String[] res = new String[result.size()];
		
		return result.toArray(res);
	}
	
	private static String removeEndDots(String s, char removal) {
		for (int i = s.length()-1; i >= 0; i--) {
			if (s.charAt(i) != removal) {
				return s.substring(0, i);
			}
		}
		return s;
	}
	
	public static List<Data> getData() throws IOException {
		String[][] data = readDataFile("DataMining2015Responses.csv",",", "-",true);
		double averageHeight = 0.0;
		int averageCount = 0;
		Data[] data2 = new Data[data.length-1];
		for (int k = 1; k < data.length; k++) {
			String[] array = data[k];
			for (int j = 0; j < array.length; j++)
				array[j] = array[j].trim();
			Data object = new Data();
			object.Age = Integer.parseInt(array[1]);
			object.isMale = array[2].equalsIgnoreCase("Male");
			object.Shoesize = Double.parseDouble(array[3].replaceAll(" ", "").replaceAll("\"", "").replaceAll(",", "."));
			object.Height = Double.parseDouble(array[4].replaceAll(",", "."));
			if (object.Height != 0.0) {
				averageHeight += object.Height;
				averageCount++;
			}
			
			//it is hard to parse the programming languages, especially when people misspell it
			String[] tempArray = array[7].split("[,;]");
			List<String> ls = new ArrayList<>();
			for (int x = 0; x < tempArray.length; x++) {
				String language = tempArray[x].replaceAll("\"", "").trim().toUpperCase();
				if (!language.isEmpty())
					if (language.endsWith("."))
						ls.add(removeEndDots(language, '.'));
					else
						ls.add(language);
			}
			object.knownProgrammingLanguages = ls.toArray(new String[ls.size()]);
			object.numberOfKnownProgrammingLanguages = ls.size();
			
			if (array[8] == null || array[8].trim().isEmpty()) object.preferredPhoneOS = PhoneOS.NONE;
			else {
				switch (array[8].trim().toUpperCase()) {
					case "WINDOWS": 
						object.preferredPhoneOS = PhoneOS.WINDOWS;
						break;
					case "ANDROID": 
						object.preferredPhoneOS = PhoneOS.ANDROID;
						break;
					case "IOS": 
						object.preferredPhoneOS = PhoneOS.IOS;
						break;
					default: object.preferredPhoneOS = PhoneOS.ANOTHER;
				}
			}
			object.likeToCodeDataminingAlgorithms = Interestingness.valueOf(array[18].toUpperCase().replaceAll(" ", "_"));
			object.likeToUseofftheshelfDataminingTools = Interestingness.valueOf(array[19].toUpperCase().replaceAll(" ", "_"));
			
			if (array[20] == null || array[20].trim().isEmpty()) object.howOftenDoYouPlayVideogames = Timeslot.UNKNOWN;
			else {
				switch (array[20].trim().toUpperCase()) {
					case "< 5 HOURS A WEEK": 
						object.howOftenDoYouPlayVideogames = Timeslot.LESS_THAN_5;
						break;
					case "< 10 HOURS A WEEK":
						object.howOftenDoYouPlayVideogames = Timeslot.LESS_THAN_10;
						break;
					case "< 20 HOURS A WEEK": 
						object.howOftenDoYouPlayVideogames = Timeslot.LESS_THAN_20;
						break;
					case "> 20 HOURS A WEEK":
						object.howOftenDoYouPlayVideogames = Timeslot.MORE_THAN_20;
						break;
					case "NEVER":
						object.howOftenDoYouPlayVideogames = Timeslot.NEVER;
						break;
					default: 
						object.howOftenDoYouPlayVideogames = Timeslot.UNKNOWN;
				}
			}
			
			//games played
			tempArray = array[21].split(",");
			List<Game> games = new ArrayList<>();
			for (String s : tempArray) {
				s = s.trim().replaceAll("[\\s:]+", "_").replaceAll("\"", "").toUpperCase();
				games.add(Game.valueOf(s));
			}
			object.gamesPlayed = games.toArray(new Game[games.size()]);
			object.numberOfGamesPlayed = games.size();
			
			List<String> pizzas = new ArrayList<>();
			pizzas.add(array[40]);
			pizzas.add(array[42]);
			pizzas.add(array[43]);
			
			object.mushroomsInTop3 = pizzas.stream().anyMatch(s -> {
					try {
						int val = Integer.parseInt(s);
						return val < 4 && val > 0 ? true : false;
					}
					catch (NumberFormatException e) {
						return false;
					}
				});	
			
			data2[k-1] = object;
		}
		averageHeight = averageHeight / averageCount;
		for (Data dat : data2) 
			if (dat.Height == 0.0)
				dat.Height = averageHeight;
		return Arrays.asList(data2);
	}
}
