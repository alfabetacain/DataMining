package prepocessing;


public class Data {
	public double Age;
	public boolean isMale;
	public double Shoesize;
	public double Height;
	public String[] knownProgrammingLanguages;
	public double numberOfKnownProgrammingLanguages;
	public PhoneOS preferredPhoneOS;
	public Interestingness likeToCodeDataminingAlgorithms;
	public Interestingness likeToUseofftheshelfDataminingTools;
	public Timeslot howOftenDoYouPlayVideogames;
	public Game[] gamesPlayed;
	public double numberOfGamesPlayed;
	public boolean mushroomsInTop3;

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Games: " + numberOfGamesPlayed + "\n");
		builder.append("Languages: " + numberOfKnownProgrammingLanguages + "\n");
		builder.append("Age: " + Age + "\n");
		builder.append("Shoe size: " + Shoesize + "\n");
		builder.append("Height: " + Height + "\n");
		return builder.toString();
	}

}