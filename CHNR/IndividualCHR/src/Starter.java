import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import pattern.Apriori;
import pattern.ItemSet;
import prepocessing.CSVFileReader;
import prepocessing.Data;
import classification.Classifier;
import classification.ID3Data;
import classification.Node;
import classification.YES_NO;
import clustering.MedoidClusterer;
import clustering.Cluster;


public class Starter {
	public static void main(String[] args) throws IOException {
		CSVFileReader reader = new CSVFileReader();
		List<Data> data = reader.getData();
		System.out.println("Dataset size: " + data.size());
		
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Classification");
		testClassifier(data);
		
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Frequent pattern");
		data = reader.getData();
		testPatternifier(data, 15, 0.75);
		
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Clustering");
		data = reader.getData();
		testClustering(data, 3);
	}
	
	public static void testClassifier(List<Data> data) {
		
		Map<Node<YES_NO>, Double> trees = new HashMap<>();
		for (int i = 0; i < 10; i++) {
			List<ID3Data<YES_NO>> ls = Classifier.transform(data);
			Collections.shuffle(ls);
			List<ID3Data<YES_NO>> training = ls.subList(0, ls.size()/3*2);
			List<ID3Data<YES_NO>> test = ls.subList(training.size(), ls.size());
			Node<YES_NO> node = Classifier.id3(training);
			//testing id3
			trees.put(node, testErrorRate(node, test));
		}
		double lowestErrorRate = Double.MAX_VALUE;
		Node<YES_NO> bestTree = null;
		for (Entry<Node<YES_NO>, Double> tree : trees.entrySet()) {
			if (tree.getValue() < lowestErrorRate) {
				lowestErrorRate = tree.getValue();
				bestTree = tree.getKey();
			}
		}
		System.out.println("---------------------------------------------------");
		System.out.println("Lowest error rate: " + lowestErrorRate);
		System.out.println("Depth: " + bestTree.depth(0));
	}
	
	private static double testErrorRate(Node<YES_NO> decisionTree, List<ID3Data<YES_NO>> testData) {
		int correct = 0;
		for (ID3Data<YES_NO> dat : testData) {
			//System.out.println("--------------------------------------------");
			YES_NO guessed = decisionTree.determineClass(dat);
			//System.out.println("Guessed class: " + guessed);
			//System.out.println("Real class: " + dat.getClassLabel());
			if (dat.getClassLabel().toString().equals(guessed.toString())) {
				//System.out.println("incrementing...");
				correct++;
			}
		}
		//System.out.println("Correct: " + correct);
		//System.out.println("Data size: " + testData.size());
		double dCorrect = correct;
		double dSize = testData.size();
		double errorRate = 1 - (dCorrect / dSize);
		System.out.println("Error rate: " + errorRate);
		System.out.println("Depth: " + decisionTree.depth(0));
		return errorRate;
	}
	
	public static void testPatternifier(List<Data> data, int supportThreshold, double miniumConfidence) {
		String[][] trans = new String[data.size()][];
		for (int i = 0; i < data.size(); i++) {
			Data entry = data.get(i);
			Arrays.sort(entry.knownProgrammingLanguages);
			trans[i] = entry.knownProgrammingLanguages;
		}
		Apriori ap = new Apriori();
		List<HashMap<ItemSet,Integer>> complete = ap.apriori(trans, supportThreshold);
		// generating association rules
        for (int i = 1; i < complete.size(); i++) {
        	for (Entry<ItemSet,Integer> superset : complete.get(i).entrySet()) {
        		for (int j = i - 1; j >= 0; j--) {
        			for (Entry<ItemSet,Integer> subset : complete.get(j).entrySet()) {
        				//if the subset is a proper subset generate association rules
        				if (!subset.getKey().equals(superset.getKey()) && Apriori.contains(subset.getKey().set, superset.getKey().set)) {
        					
        					//since the subset is a proper subset then the support of the union must be the support of the superset
    	        			double confidence = ((double) superset.getValue()) / subset.getValue();
    	        			if (confidence > miniumConfidence) {
    	        				//pretty printing, remove the subset from the superset and then print them
    	        				List<String> reducedSuperSet = new ArrayList<String>(Arrays.asList(superset.getKey().set.clone()));
    	        				reducedSuperSet.removeAll(Arrays.asList(subset.getKey().set));
    	        				String[] array = (String[]) reducedSuperSet.toArray(new String[reducedSuperSet.size()]);
    	        				Arrays.sort(array);
    	        				ItemSet newSet = new ItemSet(array); //make it an itemset just to get the pretty tostring method
    	        				System.out.println(subset.getKey() + "(" + subset.getValue() + ")" +  " -> " + newSet + "(" + superset.getValue() + ")" + "=" + confidence);
    	        			}
        				}
        			}
        		}
        	}
        }
	}
	
	public static void testClustering(List<Data> data, int numberOfClusters) {
		MedoidClusterer clusterer = new MedoidClusterer(data);
		List<Cluster> clusters = clusterer.KMedoidPartition(numberOfClusters);
		//clusters.stream().forEach(clust -> System.out.println(clust));
		
		//validity metrics
		//farthest and average intercluster distance
		for (int i = 0; i < clusters.size(); i++) {
			Cluster cluster = clusters.get(i);
			System.out.println("-----------------------CLUSTER " + i + " START------------------------------------");
			double maxDist = 0.0;
			double average = 0.0;
			int averageCount = 0;
			for (Data first : cluster.ClusterMembers) {
				for (Data second : cluster.ClusterMembers) {
					double dist = MedoidClusterer.distance(first, second);
					if (dist > maxDist) 
						maxDist = dist;
					if (dist > 150.0) {
						System.out.println(first);
						System.out.println(second);
					}
					average += dist;
					averageCount++;
				}
			}
			for (Cluster cluster2 : clusters) {
				if (cluster == cluster2)
					continue;
				double medDist = MedoidClusterer.distance(cluster.Medoid, cluster2.Medoid);
				System.out.println("Distance to " + clusters.indexOf(cluster2) + ": " + medDist);
			}
			System.out.println("Cluster size: " + cluster.ClusterMembers.size());
			System.out.println("Max intercluster distance: " + maxDist);
			System.out.println("Average intercluster distance: " + average/averageCount);
			System.out.println("-----------------------CLUSTER " + i + " END-------------------------");
		}
	}
}