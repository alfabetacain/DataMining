package clustering;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import prepocessing.Data;

public class MedoidClusterer {

	private final List<Data> normalizedData;
	
	public MedoidClusterer(List<Data> data) {
		
		//normalize numerical data, so one attribute does not weigh more than the others
		Data[] norm = (Data[]) data.toArray(); //safe copy
		double minHeight = Double.MAX_VALUE;
		double maxHeight = Double.MIN_VALUE;
		double minAge = Double.MAX_VALUE;
		double maxAge = Double.MIN_VALUE;
		double minShoe = Double.MAX_VALUE;
		double maxShoe = Double.MIN_VALUE;
		double minGames = Double.MAX_VALUE;
		double maxGames = Double.MIN_VALUE;
		double minLang = Double.MAX_VALUE;
		double maxLang = Double.MIN_VALUE;
		for (Data dat : norm) {
			if (dat.Height < minHeight)
				minHeight = dat.Height;
			if (dat.Height > maxHeight)
				maxHeight = dat.Height;
			
			if (dat.Age < minAge)
				minAge = dat.Age;
			if (dat.Age > maxAge)
				maxAge = dat.Age;
			
			if (dat.Shoesize < minShoe)
				minShoe = dat.Shoesize;
			if (dat.Shoesize > maxShoe)
				maxShoe = dat.Shoesize;
			
			if (dat.numberOfGamesPlayed < minGames)
				minGames = dat.numberOfGamesPlayed;
			if (dat.numberOfGamesPlayed > maxGames)
				maxGames = dat.numberOfGamesPlayed;
			
			if (dat.numberOfKnownProgrammingLanguages < minLang)
				minLang = dat.numberOfKnownProgrammingLanguages;
			if (dat.numberOfKnownProgrammingLanguages > maxLang)
				maxLang = dat.numberOfKnownProgrammingLanguages;
		}
		//normalize data to be between 0 and 10
		for (Data dat : norm) {
			dat.Height = normalize(dat.Height, minHeight, maxHeight, 0, 1);
			dat.Age = normalize(dat.Age, minAge, maxAge, 0, 1);
			dat.Shoesize = normalize(dat.Shoesize, minShoe, maxShoe, 0, 1);
			dat.numberOfGamesPlayed = normalize(dat.numberOfGamesPlayed, minGames, maxGames, 0, 1);
			dat.numberOfKnownProgrammingLanguages = normalize(dat.numberOfKnownProgrammingLanguages, minLang, maxLang, 0, 1);
		}
		normalizedData = Arrays.asList(norm);
	}
	
	public List<Cluster> KMedoidPartition(int k)
	{
		//if there are no normalized data, we cannot cluster
		if (k > normalizedData.size()) {
			return null;
		}
		
		//assign random samples as medoids
		Random rand = new Random();
		HashSet<Data> randoms = new HashSet<>();
		while (randoms.size() < k) {
			Data next = normalizedData.get(rand.nextInt(normalizedData.size()-1));
			if (!randoms.contains(next))
				randoms.add(next);
		}
		
		List<Cluster> clusters = new ArrayList<>();
		for (Data dat : randoms) {
			clusters.add(new Cluster(dat));
		}
		
		boolean changing = true;
		while (changing) { //will terminate when no new assignments have been done
			changing = false;
			//assign each sample to the closest medoid cluster
			for (Data current : normalizedData) {
				double minDist = Double.MAX_VALUE;
				Cluster closestCluster = null;
				for (Cluster cluster : clusters) {
					if (cluster.ClusterMembers.contains(current)) //might as well remove it, since we will add it somewhere later
						cluster.ClusterMembers.remove(current);
		
					double dist = distance(current,cluster.Medoid);
					if (dist < minDist) {
						//found new shortest distance
						minDist = dist;
						closestCluster = cluster;
					}
				}
				//add the element, if no better medoid has been found than the previous
				//it will just be assigned to the same cluster
				closestCluster.ClusterMembers.add(current); 
			}
			//see if there are better medoids
			for (Cluster cluster : clusters) {
				double lowestCost = Double.MAX_VALUE;
				Data newMedoid = null;
				//find the medoid with the lowest swap cost
				for (Data sample : cluster.ClusterMembers) {
					double sampleCost = swapCost(sample, cluster);
					if (sampleCost < lowestCost) {
						lowestCost = sampleCost;
						newMedoid = sample;
					}
				}
				//if a new medoid has been found, set the boolean to true, so the algorithm will run another round
				if (newMedoid != cluster.Medoid) {
					cluster.Medoid = newMedoid;
					changing = true;
				}
			}
		}
		return clusters; //return the clusters when done
		
	}
	
	/**
	 * Calculates the euclidean distance between two data samples
	 * @param dat1 sample 1
	 * @param dat2 sample 2
	 * @return the euclidean distance
	 */
	public static double distance(Data dat1, Data dat2) {
		double dist = 0.0;
		dist += Math.pow(dat1.numberOfKnownProgrammingLanguages - dat2.numberOfKnownProgrammingLanguages,2);
		dist += Math.pow(dat1.numberOfGamesPlayed - dat2.numberOfGamesPlayed,2);
		dist += Math.pow(dat1.Age - dat2.Age, 2);
		dist += Math.pow(dat1.Shoesize - dat2.Shoesize, 2);
		dist += Math.pow(dat1.Height - dat2.Height, 2);
		return Math.sqrt(dist);
	}
	
	/**
	 * Calculates the swap cost, the average euclidean distance to the pretender
	 * @param pretender the sample from which to calculate the distances
	 * @param cluster the cluster which contains the other data samples
	 * @return the swap cost, average euclidean distance to pretender
	 */
	private static double swapCost(Data pretender, Cluster cluster) {
		double average = 0.0;
		//sums the averages, does not include the medoid itself
		for (Data current : cluster.ClusterMembers)
			if (current != pretender)
				average += distance(pretender,current);
		return average / cluster.ClusterMembers.size()-1;
	}	
	
	//normalizes the value
	private double normalize(double value, double min, double max, double newMin, double newMax) {
		return ((value - min) / (max - min)) * (newMax - newMin) + newMin;
	}
}
