package clustering;
import java.util.HashSet;
import java.util.Set;

import prepocessing.Data;

public class Cluster {

public Set<Data> ClusterMembers;
public Data Medoid;
	
//based on the lab sessions
public Cluster(Data medoid)
{
	this.ClusterMembers = new HashSet<Data>();
	ClusterMembers.add(medoid);
	this.Medoid = medoid;
}
	
	@Override
	public String toString() {
		String toPrintString = "-----------------------------------CLUSTER START------------------------------------------" + System.getProperty("line.separator");
		toPrintString += "Medoid: "+this.Medoid.toString() + System.getProperty("line.separator");
		for(Data i : this.ClusterMembers)
		{
			toPrintString += i.toString() + System.getProperty("line.separator");
		}
		toPrintString += "-----------------------------------CLUSTER END-------------------------------------------" + System.getProperty("line.separator");
		
		return toPrintString;
	}
	
}
