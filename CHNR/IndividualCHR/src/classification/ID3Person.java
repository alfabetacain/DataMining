package classification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import prepocessing.Data;
import prepocessing.Game;
import prepocessing.Interestingness;
import prepocessing.PhoneOS;
import prepocessing.Timeslot;

/**
 * Implementation of ID3Data for use with ID3. The data samples must have this
 * format, so we preprocess the data, so it will be in accordance with the ID3Data interface
 * @author christian
 *
 */
public class ID3Person implements ID3Data<YES_NO> {
	private final YES_NO classLabel;
	private final Map<String,Object> attMap;
	private static Map<String,Object> attributes;
	static {
		attributes = new HashMap<>();
		attributes.put("Is male",YES_NO.class);
		attributes.put("Preferred phone os", PhoneOS.class);
		attributes.put("like to code data mining algorithms", Interestingness.class);
		attributes.put("like to use off the shelf data mining tools", Interestingness.class);
		attributes.put("how often do you play video games", Timeslot.class);
		
		for (Game game : Game.values()) {
			attributes.put("have played " + game, YES_NO.class);
		}
	}
	
	/**
	 * Constructs an ID3Person by using the values in the original data sample
	 * @param data The original sample to base this object on
	 */
	public ID3Person(Data data) {		
		attMap = setupAttMap(data);
		classLabel = boolToYes_No(data.mushroomsInTop3);
	}
	
	/**
	 * Sets up the attributes in accordance with the ID3Data interface.
	 * Basically converts all non-enum data to enums or ignores them
	 * @param data
	 * @return
	 */
	private Map<String,Object> setupAttMap(Data data) {
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("Is male", boolToYes_No(data.isMale));
		map.put("Preferred phone os", data.preferredPhoneOS);
		map.put("like to code data mining algorithms", data.likeToCodeDataminingAlgorithms);
		map.put("like to use off the shelf data mining tools", data.likeToUseofftheshelfDataminingTools);
		map.put("how often do you play video games", data.howOftenDoYouPlayVideogames);
		//games
		for (Game game : Game.values()) {
			map.put("have played " + game, Arrays.asList(data.gamesPlayed).contains(game) ? YES_NO.YES : YES_NO.NO);
		}
		return map;
	}
	
	private YES_NO boolToYes_No(boolean bool) {
		return bool ? YES_NO.YES : YES_NO.NO;
	}
	
	@Override
	public Map<String,Object> getAttributes() {
		return attributes;
	}
	
	@Override
	public Object getAttributeValue(String key) {
		return attMap.getOrDefault(key, null);
	}
	
	@Override
	public YES_NO getClassLabel() {
		return classLabel;
	}
	
	@Override
	public List<Object> getAllPossibleValues(String key) {
		Object type = attMap.get(key);
		if (type instanceof YES_NO) {
			List<Object> obs = new ArrayList<>();
			obs.add(YES_NO.YES);
			obs.add(YES_NO.NO);
			return obs;
		}
		if (type instanceof Interestingness) {
			return Arrays.asList((Object[]) Interestingness.values());
		}
		if (type instanceof Timeslot) 
			return Arrays.asList((Object[]) Timeslot.values());
		if (type instanceof PhoneOS)
			return Arrays.asList((Object[]) PhoneOS.values());
		return new ArrayList<Object>();
	}
}
