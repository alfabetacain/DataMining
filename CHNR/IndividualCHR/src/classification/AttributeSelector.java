package classification;


import java.util.List;
import java.util.Map;

/**
 * Interface for defining an attribute selector
 * @author christian
 *
 * @param <T>
 */
public interface AttributeSelector<T> {
	public String select(List<ID3Data<T>> data, Map<String, Object> attributes);
}
