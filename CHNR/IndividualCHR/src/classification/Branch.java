package classification;

/**
 * Class representing a branch in the decision tree
 * Contains the value for the branch, the name of the attribute it belongs to and 
 * the node it links to.
 * @author christian
 *
 * @param <R> The type of the attribute value, typically just an object
 * @param <T> The class label type of the node
 */
public class Branch<R,T> {
	private R branchValue;
	private String attName;
	private Node<T> node;
	
	public Branch(R cond, String attName, Node<T> node) {
		branchValue = cond;
		this.node = node;
		this.attName = attName;
	}
	
	public String getAttName() {
		return attName;
	}
	
	public R getBranchCondition() {
		return branchValue;
	}
	
	public String getCondition() {
		return branchValue.toString();
	}
	
	public Node<T> getNode() {
		return node;
	}
}
