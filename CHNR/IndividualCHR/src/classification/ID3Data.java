package classification;

import java.util.List;
import java.util.Map;

//interface for data types to be used in ID3
public interface ID3Data<T> {
	/**
	 * Returns a list of all attributes to be used by ID3. Could have been static, but java does not allow static methods in interfaces
	 * @return a list of all available attributes
	 */
	public Map<String,Object> getAttributes();
	/**
	 * Gets the value of an attribute with the given name (the key)
	 * @param key The name of the attribute
	 * @return the sample's value for the given attribute
	 */
	public Object getAttributeValue(String key);
	/**
	 * Returns the class label of the data sample
	 * @return the class label of the data sample
	 */
	public T getClassLabel();
	
	/**
	 * Returns all possible values for a given attribute name/key. Used when splitting on the attribute
	 * @param key the name of the attribute
	 * @return all possible values for the given attribute
	 */
	public List<Object> getAllPossibleValues(String key);
}
