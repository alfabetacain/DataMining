package classification;

/**
 * Class for holding an attribute and the gain associated with the attribute
 * Simply to make it easier to compare information gains
 * @author christian
 *
 */
public class Container implements Comparable<Container> {
	public String attribute;
	public double gain;
	@Override
	public int compareTo(Container o) {
		// TODO Auto-generated method stub
		if (gain < o.gain)
			return -1;
		if (gain > o.gain)
			return 1;
		return 0;
	}
}
