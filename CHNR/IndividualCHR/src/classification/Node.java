package classification;
import java.util.ArrayList;
import java.util.List;

/**
 * Node class for the decision tree. A node holds a class label or a list of branches. 
 * If the list of branches is null, then it is a leaf node and the class label determines the type.
 * If the list is not null, then the label should not be used, but the branches should be used to determine
 * what node to go to next
 * @author christian
 *
 * @param <T> type of class label
 */
public class Node<T> {
	//list of branches, empty is node is leaf
	public List<Branch<Object,T>> branches = new ArrayList<>();
	//class label, null if node is not leaf
	public T label;
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		if (branches.size() == 0) {
			builder.append("\r\nLeaf node -> Class: ");
			builder.append(label.toString());
		}
		else {
			builder.append("Not leaf node");
			for (Branch<Object,T> b : branches) {
				builder.append("\r\nBranch -> Condition = " + b.getAttName() + " - " + b.getCondition() + " -> " + b.getNode().toString());
			}
		}
		return builder.toString();
	}
	
	/**
	 * Determines the class of a given data sample by sending it through the decision tree
	 * The data sample must have the same class label type as the node
	 * @param data
	 * @return
	 */
	public T determineClass(ID3Data<T> data) {
		if (branches.isEmpty()) {
			//System.out.println("Is leaf node, returning " + label);
			return label;
		}

		//System.out.println("Question: " + branches.get(0).getAttName());
		for (Branch<Object,T> branch : branches) {
			Object myVal = data.getAttributeValue(branch.getAttName());
			//System.out.println("Myval: " + myVal + " == " + branch.getBranchCondition());
			//checks if the data sample has the same value as the branch
			if (branch.getBranchCondition().equals(data.getAttributeValue(branch.getAttName()))) {
				//System.out.println("Found match: " + branch.getBranchCondition());
				return branch.getNode().determineClass(data);
			}
		}
		return null; //something went wrong
	}
	
	public int depth(final int count) {
		if (branches.isEmpty())
			return count+1;
		else
			return branches.stream().mapToInt(b -> b.getNode().depth(count+1)).max().getAsInt();
	}
}
