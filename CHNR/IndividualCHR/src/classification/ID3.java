package classification;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class ID3<T> {
	
	public Node<T> buildTree(List<ID3Data<T>> data, Map<String, Object> attributes, AttributeSelector<T> selector) {
		//build root node
		Node<T> node = new Node<T>();
		if (hasSameClass(data)) {
			//all data samples have the same class label
			node.label = data.get(0).getClassLabel();
			return node;
		}
		
		if (attributes.isEmpty()) {
			//no more attributes to split on, majority vote
			node.label = getMajorityClass(data);
			return node;
		}
		//choose the best attribute to split on
		String att = selector.select(data, attributes);
		
		//get new attribute list, everything except the splitting attribute
		List<String> filteredAttributes = new ArrayList<>();
		for (String other : attributes.keySet()) {
			if (!att.equals(other))
				filteredAttributes.add(other);
		}

		//make new map to prevent accidental updates of the old map
		Map<String, Object> filtered = new HashMap<>();
		for (String key : filteredAttributes)
			filtered.put(key, attributes.get(key));
		
		//split into a branch for each possible value of the splitting attribute
		for (Object obj : data.get(0).getAllPossibleValues(att)) {
			//get all data that has the given attribute value
			List<ID3Data<T>> branchData = data.stream().filter(mush -> mush.getAttributeValue(att).equals(obj)).collect(Collectors.toList());
			
			if (branchData.isEmpty()) {
				//if no data samples have the given value, make a leaf node and assign it the majority class of all the data at this step
				Node<T> leafNode = new Node<T>();
				leafNode.label = node.label = getMajorityClass(data);
				node.branches.add(new Branch<Object,T>(obj,att, leafNode));
			}
			else
				node.branches.add(new Branch<Object,T>(obj,att, buildTree(branchData, filtered, selector))); //else make new branch and repeat algorithm on the next step
		}
		return node;
			
	}
	
	//determines if every data sample has the same class label by comparing the samples pairwise from left to right
	private boolean hasSameClass(List<ID3Data<T>> data) {
		for (int i = 1; i < data.size(); i++) {
			if (!data.get(i-1).getClassLabel().equals(data.get(i).getClassLabel()))
				return false;
		}
		return true;
	}
		
	//determines the majority class label of a list of data samples
	private T getMajorityClass(List<ID3Data<T>> data) {
		//make map and save all class label values in it and the number of samples with the given class label
		Map<T,Integer> map = new HashMap<T,Integer>();
		for (ID3Data<T> dat : data) {
			Integer old = map.get(dat.getClassLabel());
			if (old == null)
				map.put(dat.getClassLabel(), 1);
			else
				map.put(dat.getClassLabel(), old+1);
		}
		
		//choose the class label with the highest support
		T majority = null;
		Integer max = 0;
		for (T type : map.keySet()) {
			if (map.get(type) > max) {
				max = map.get(type);
				majority = type;
			}
		}
		return majority;
	}
}
