package classification;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import prepocessing.Data;

/**
 * Class for using the ID3 algorithm. Also has method for converting from Data class to ID3Data
 * some program structure taken from lab sessions
 * @author christian
 *
 */
public class Classifier {

	public static Node<YES_NO> id3Pre(List<Data> data) {
		return id3(transform(data));
	}
	public static Node<YES_NO> id3(List<ID3Data<YES_NO>> normalizedData) {
		
		ID3<YES_NO> brancher = new ID3<YES_NO>();
		//initialize the algorithm with all possible attributes and use the given method for selecting the best splitting attribute
		Node<YES_NO> node = brancher.buildTree(normalizedData, normalizedData.get(0).getAttributes(), new AttributeSelector<YES_NO>() {
			//selects the best splitting attribute using the given data and attribute list
			@Override
			public String select(List<ID3Data<YES_NO>> data, Map<String, Object> attributes) {
				//calculate expected info, entropy
				double infoD = infoD(data);
				//list of gains for attributes
				List<Container> attributeGains = new ArrayList<>();
				for (String att : attributes.keySet()) {
					Container cont = new Container();
					cont.attribute = att;
					cont.gain = infoD - infoA(att,data); //calculate the gain
					attributeGains.add(cont);
				}
				//find the max gain
				return Collections.max(attributeGains).attribute;
			}});
		return node;
	}
	
	public static double infoD(List<ID3Data<YES_NO>> data) {
		double infoD = 0.0;
		for (YES_NO label : YES_NO.values()) {
			//number of samples which have the given class label
			double matches = data.stream().filter(dat -> dat.getClassLabel().equals(label)).count();
			double probability = matches / data.size();
			infoD -= probability * log2(probability);
		}
		return infoD;
	}
	
	private static double infoA(String splitAttributeCandidate, List<ID3Data<YES_NO>> data) {
		double infoA = 0.0;
		for (Object attVal : data.get(0).getAllPossibleValues(splitAttributeCandidate)) {
			//samples which have the attVal value for the splitAttributeCandidate
			List<ID3Data<YES_NO>> myShare = data.stream().filter(dat -> dat.getAttributeValue(splitAttributeCandidate).equals(attVal)).collect(Collectors.toList());
			infoA += myShare.size() / data.size() * infoD(myShare);
		}
		return infoA;
	}
	
	private static double log2(double value) {
		return Math.log(value) / Math.log(2); //is probably imprecise
	}
	
	public static List<ID3Data<YES_NO>> transform(List<Data> data) {
		return data.stream().map(dat -> new ID3Person(dat)).collect(Collectors.toList());
	}

}
