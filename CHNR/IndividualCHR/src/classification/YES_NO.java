package classification;

/**
 * enum for simple no/yes answers. Could have used a boolean but decided to restrict ID3 to only use enums
 * Used for many different questions
 * @author christian
 *
 */
public enum YES_NO {
	YES,NO;
}
