# Individual DataMining Project.
				Author
		Mads Ljungberg : Malj@itu.dk
				27-03-2015

	
# The Question
 *	I want to answer these questions.
 
	What are the most played games on the MDMI class: 
	[if you have played Angry Birds did you play Candy Crush as well]?
	using __Apriori__.
	
	Is there any interesting groups in the dataset?
	fx. given a age, height and shoesize you belong to a group, 
	only with students with a special degree.
	using __K-Means__.
	
	Does certain choices in the questionnaire decide your gender?
	fx. have answered your degree, 
	and how often you play then depending on the answer you are a Male or Female.
	using __ID3__.
	

# Methods

 * **Preprocessing** \newline
	Because of the small dataset I have tried as much as I could to keep all Columns/attributes present.
	
	For missing values in the given form: Numerical, Ordinal, 
	and Nominal I have handled the replacement of the values differently.
	Numerical values are handled by taking the mean.
	Ordinal values are handled by taking the mean since my values ends as numbers, 
	but one could argue that this is wrong. 
	Since no one can say there is a equal gap from one answer to another.
	Nominal values are not handled.
	
	I have written my algorithms on the assumption that the data provided to the algorithms is clean. 
	Free of missing values and dirty values on the last topic I have had some issues finding the best way to clean it,
	without degrading the value present to a missing value. Some of the Columns/attributes has not been cleaned.
	
 * **ID3 preprocessing** \newline
	ID3 as of now the algorithm only classify on nominal values I have tried to think of a way,
	to include Ordinal values but have not found a solution yet due to time. 
	Numerical values I could have introduced as intervals that fitted the data. 
	("height fx. could be < 100 cm , < 150 < 200 etc.") but the intervals should be smaller.
	
 * **Apriori** \newline
	Apriori as of now the algorithm calculates the frequent sets of games played. 
	This colum needed some work to work with the apriori algorithm, 
	the dataset given delivered games played as one string that needed to be split up into array/set.
	
# Results

 * **Apriori** \newline
	The frequent most played games with their support the higher support the more frequent : 
	[Stanley parable]=19, ["Minecraft]=24, [Counter strike: GO]=25, [Wordfeud]=25, 
	["Fifa 2014]=13, [Fifa 2014]=16, [Minecraft]=11, [Grand theft auto V"]=18, 
	[Last of us"]=13, [Call of duty: modern warfare]=21, [I have not played any of these games]=10, 
	[Angry birds]=27, ["Candy crush]=17, [Angry birds"]=24]
	As you can see my data has not been cleaned properly. 
	So the result can not tell us anything interesting.
	
 * **K-Means** \newline
	I had trouble with this algorithm since it would only cluster into one cluster.
	Why it could have something to do with the columns I used the variation in value was not that big.
	Solution introduce more varied columns to the algorithm.
	
 * **ID3** \newline
	I have tried to build a tree using all of the dataset to see if something stood out.
	on that note it looks like there is a special rout where the last question is about the PhoneOS,
	and if you picked a apple based OS then you would be categorised as a Female.
	
	But on the other hand nothing conclusive can be told of the tree.

# Conclusion

* 	All in all this project has proven to be limited in its answers.
	This I blame on the limited dataset, and my confusions in the algorithms 
	due to time constraints,
	given more time this could prove interesting, but might still not tell us anything.
	