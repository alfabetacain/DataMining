import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import dk.mads.CSVFileReader.CSVFileReader;
import dk.mads.algorithms.Cleaning;
import dk.mads.algorithms.MapObject;
import dk.mads.algorithms.Apriori.Apriori;
import dk.mads.algorithms.ID3.RunID3;
import dk.mads.algorithms.KMean.KMeanCluster;
import dk.mads.algorithms.KMean.KMeans;
import dk.mads.data.Response;


public class Main {
	
	private static final Logger logger =
	        Logger.getLogger(Main.class.getName());

	public static void main(String[] args) {
		logger.setLevel(Level.INFO);		
		logger.log(Level.INFO, "Starting Application");
		
		// Read CSVFile.
			HashMap<String,HashMap<Integer,String[]>> dataToClean = CSVFileReader.runReader();
			
		// Clean Data.
			dataToClean = Cleaning.cleanData(dataToClean);
		
		// MAP Data to objects.
			ArrayList<Response> responses = new ArrayList<>();
			responses = MapObject.mapToObject(dataToClean);
		
		// Pre Algorithm Preperations
			
			String[][] aprioriData = prepApriori(responses);
			
		// Run Algorithms.
			RunID3.run(responses);
			
			Apriori.run(aprioriData);
					
			ArrayList<KMeanCluster> cluster = KMeans.KMeansPartition(6, responses);
			
		// Post Algorithm Handling
			postKMean(cluster);

		logger.log(Level.INFO, "Stopping Application");
	}		
	
	private static void postKMean(ArrayList<KMeanCluster> cluster)
	{
		int clusters = cluster.size();
		System.out.println(clusters);
		System.out.println(cluster.get(0));
		for (int i = 0 ; i < clusters; i++)
		{
			cluster.get(i).toString();
		}
	}
	
	private static String[][] prepApriori(ArrayList<Response> responses)
	{
		int count = 0;
		String[][] AprioriData = new String[responses.size()][];
		for (Response resp : responses)
		{
			AprioriData[count] = resp.gamesplayed;
			count++;
		}
		return AprioriData;
	}
}
