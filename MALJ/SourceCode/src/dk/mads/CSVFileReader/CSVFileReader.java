package dk.mads.CSVFileReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CSVFileReader {
	
	private static final Logger logger =
	        Logger.getLogger(CSVFileReader.class.getName());
	
	private static HashMap<String,HashMap<Integer,String[]>> readDataFile(String csvFile, String seperationChar, String nullValue, boolean checkForQuoteMarkRange) throws IOException
	{
		logger.log(Level.INFO, "Gathering Data From CSVFile");
		
		HashMap<Integer, String[]> missing = new HashMap<>();
		HashMap<Integer, String[]> goodset = new HashMap<>();
		HashMap<String,HashMap<Integer,String[]>> dataset = new HashMap<>();
		
		BufferedReader bufRdr = new BufferedReader(new FileReader(new File(csvFile)));
		
		// Read the header.
		String line = bufRdr.readLine();
		int count = 0;
		while ((line = bufRdr.readLine()) != null) {
			
			String[] arr = line.split(seperationChar); 
			
			boolean hasMissingValue = false;
			
			for(int i = 0; i < arr.length; i++)
			{
				if(arr[i].equals(""))
				{
					arr[i] = nullValue;
					hasMissingValue = true;
				}				
			}
			
			if(checkForQuoteMarkRange)
			{
				arr = CheckForQuoteMarkRange(arr);
			}

			if (!hasMissingValue)
			{
				goodset.put(count, arr);
				count++;
			}
			else
			{
				missing.put(count, arr);
			}
			
			count++;
		}
		
		dataset.put("missing", missing);
		dataset.put("goodset", goodset);
		bufRdr.close();
		
		logger.log(Level.INFO, "Done Gathering Data");
		logger.log(Level.FINE, "Found " + missing.size() + " entries with missing inputs");
		logger.log(Level.FINE, "Found " + goodset.size() + " entries with non missing inputs");
		
		return dataset;
	}

	private static String[] CheckForQuoteMarkRange(String[] line)
	{
		ArrayList<String> result = new ArrayList<String>();
		ArrayList<String> keeper = new ArrayList<String>();
		boolean addToKeeper = false;
		
		for(String part : line)
		{
			if(part.contains("\""))
			{
				if(addToKeeper)
				{
					addToKeeper = false;
					keeper.add(part);					
					String toAdd = keeper.toString();
					result.add(toAdd.substring(1, toAdd.length()-1));//Done to remove brackets from the string.
					keeper.clear();
				}
				else
				{
					addToKeeper = true;
					keeper.add(part);
				}
			}
			else
			{
				if(addToKeeper)
				{
					keeper.add(part);
				}
				else
				{
					result.add(part);
				}
			}
		}
		String[] res = new String[result.size()];
		
		return result.toArray(res);
	}


	public static HashMap<String,HashMap<Integer,String[]>> runReader()
	{
		HashMap<String,HashMap<Integer, String[]>> data = new HashMap<>();
		
		try 
		{
			data = readDataFile("DataMining2015Responses.csv",",", "-",true);
		}
		catch (IOException ioe)
		{
			System.out.printf("Following error occured: %s", ioe );
		}
		return data;
	}
}