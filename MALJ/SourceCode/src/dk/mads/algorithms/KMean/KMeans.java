package dk.mads.algorithms.KMean;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import dk.mads.data.Response;


public class KMeans {

	private static final Logger logger = 
			Logger.getLogger(KMeans.class.getName());
	
	public static ArrayList<KMeanCluster> KMeansPartition(int k, ArrayList<Response> data)
	{
		ArrayList<KMeanCluster> clusters = new ArrayList<>(k);
		
		for (int i = 0; i < k; i++)
		{
			KMeanCluster cluster = new KMeanCluster();
			cluster.ClusterMembers.add(data.get(i));
			clusters.add(cluster);
		}
		
		boolean notChanged = false;
		
		while(!notChanged)
		{
			notChanged = true;
			HashMap<KMeanCluster,Response> means = new HashMap<>();
			for(KMeanCluster cluster : clusters) 
			{
				means.put(cluster,calculateMean(cluster.ClusterMembers));
			}
			for(Response resp : data)
			{
				double shortestDistance = Double.MAX_VALUE;
				KMeanCluster shortestCluster = null;
				
				for(KMeanCluster cluster : clusters)
				{
					double dist = distance(resp,means.get(cluster));
					
					if(dist < shortestDistance)
					{
						shortestDistance = dist;
						shortestCluster = cluster;
					}
				}
				if (!shortestCluster.ClusterMembers.contains(resp))
				{
					shortestCluster.ClusterMembers.add(resp);
					notChanged = false;
				}
			}
		}
		
		return clusters;
	}
	
	private static double distance(Response response1, Response response2)
	{
		double dist = Math.pow(response1.Height - response2.Height,2);
		dist += Math.pow(response1.Age - response2.Age,2);
		dist += Math.pow(response1.Shoesize - response2.Shoesize, 2);
		logger.log(Level.FINE, "Distance calculated: " + dist);

		return Math.sqrt(dist);
	}
	
	private static Response calculateMean(ArrayList<Response> data)
	{
		// Add more values. TODO Handle.
		float height;
		float age;
		float shoesize;
		height = age = shoesize = 0f;
		
		for(Response resp : data)
		{
			height += resp.Height;
			age += resp.Age;
			shoesize += (float) resp.Shoesize;
		}
		int count = data.size();
		Response response = new Response();
		
		// set up only needed values.
		response.Height = (int) height/count;
		response.Age	= (int) age/count;
		response.Shoesize = (double) shoesize/count;
		
		return response;
	}
}
