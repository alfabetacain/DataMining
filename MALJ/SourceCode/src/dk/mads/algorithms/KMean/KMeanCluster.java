package dk.mads.algorithms.KMean;
import java.util.ArrayList;

import dk.mads.data.Response;

// Compute cluster mean based on cluster members.
public class KMeanCluster {

	public ArrayList<Response> ClusterMembers;
	
	public KMeanCluster()
	{
		this.ClusterMembers = new ArrayList<>();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		String clusterStartString = "-----------------------------------CLUSTER START------------------------------------------" + System.getProperty("line.separator");
		sb.append(clusterStartString);
		for(Response i : this.ClusterMembers)
		{
			sb.append("Age = " + i.Age + "\n");
			sb.append("Height = " + i.Height + "\n");
			sb.append("Shoesize = " + i.Shoesize + "\n");
			sb.append("Degree = " + i.Whatdegreeareyoustudying + "\n");
//			sb.append(i + "\n");
			
		}
		String clusterEndString = "-----------------------------------CLUSTER END-------------------------------------------" + System.getProperty("line.separator");
		
		sb.append(clusterEndString);
		return sb.toString();
	}

}
