package dk.mads.algorithms;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Cleaning {
	
	private static final Logger logger =
	        Logger.getLogger(Cleaning.class.getName());
	private static final int magicNumber = 49;
	private static HashMap<Integer, int[]> attributecollection = new HashMap<>();
	private static HashMap<Integer, Integer> defaultvalues = new HashMap<>();

	public static HashMap<String, HashMap<Integer,String[]>> cleanData(HashMap<String, HashMap<Integer,String[]>> dataToClean){
		HashMap<Integer,String[]> goodset = dataToClean.get("goodset");
		HashMap<Integer,String[]> missing = dataToClean.get("missing");
		
		//Detect attributes for the missing values.
		
		detectMissingAttributeLocations(missing);

		// Determine default value for missing attributes
		// and add them to the respectful places.
		
		handleDefaultvalues(goodset, missing);
		
		
		// Merge Solved Missing Values with the goodset.
		Set<Integer> transferKeys = missing.keySet();
		
		for (Integer transferKey : transferKeys)
		{
			goodset.put(transferKey, missing.get(transferKey));
		}
		
		// Normalize data 

		// Currently exists in MapObjects should be handled here. TODO
		
		
		return dataToClean;
	}

	
	
	private static void handleDefaultvalues(HashMap<Integer,String[]> goodset, HashMap<Integer,String[]> missing)
	{
		readAttributes();
		
		// Get the Keys From The missing attribute location mapping
		Set<Integer> keys = attributecollection.keySet();

		// Iterate over every entry with missing values
		for (Integer key : keys)
		{			
			// Fetch The locator Array to map what value at what attribute is missing
			int[] missingvalues = attributecollection.get(key);
			
			for (int i = 0; missingvalues[i] > 0; i++)
			{	
				// Default value initiator
				if( defaultvalues.get(missingvalues[i]) == null)
				{
					logger.log(Level.FINER, "Adding : " + missingvalues[i] + " to: " + 
							defaultvalues.keySet().toString() + " with keyset size of: " + 
							defaultvalues.keySet().size());
					
					defaultvalues.put(missingvalues[i], 0);
				}
				
				// Calculate default values to replace missing values with
				calculatedefaultvalues(goodset);
				
				// Add the calculated value to the entries with missing values.
				String[] missingvalue = missing.get(key);
				
				missingvalue[missingvalues[i]] = defaultvalues.get(missingvalues[i]).toString();
				
				missing.put(key, missingvalue);
			}
			
		}
	}
	
	private static void calculatedefaultvalues(HashMap<Integer,String[]> goodset)
	{
		// Fetch the Entries from missing value collection and 
		// Entries on the set that needs to be used to calculate default values.
		Set<Integer> defaultkeys = defaultvalues.keySet();
		Set<Integer> setKeys = goodset.keySet();
			
			// Count how many Good Entries we got.
			int count = setKeys.size();		
			
			// For every missing entry
			for(Integer item : defaultkeys)
			{	
				logger.log(Level.FINEST, "Default keys: " + defaultkeys.toString());

					//take the temporary default value and add to it.	
					int temp = defaultvalues.get(item);
					
					logger.log(Level.FINE, "Default value on: " + item + " is: " + temp + "\n");
					
					//Iterate through all data and collect needed values for average calculation
					for (Integer key : setKeys)
					{
						logger.log(Level.FINEST, "Handling entry: " + key + " for missing value: " + item + "\n");

						temp = defaultvalues.get(item) + Integer.parseInt(goodset.get(key)[item]);
						defaultvalues.put(item, temp);
						
					}
					
					// Calculate average value
					temp = defaultvalues.get(item) / count;
					
					// Add the average to the collection.
					defaultvalues.put(item, temp);
				}
			
			logger.log(Level.FINE, "Default values: " + defaultvalues.values());
			
	}
	
	private static void detectMissingAttributeLocations(HashMap<Integer,String[]> missing)
	{		
		Set<Integer> temp = missing.keySet();
		
		// For every key in missing [Key=Entry][Value=Missingvalue]
		for (Integer o : temp)
		{
			// Get the value for entry o
			String[] attributeData = missing.get(o);
			int[] attributeLocation = new int[attributeData.length];
			int count = 0;
			for (int i = 0 ; i < attributeData.length; i++)
			{
				// If AttributeData has a null value on location i then add it to the list of missing values
				if (attributeData[i] == "-")
				{
					attributeLocation[count] = i;
					count++;
				}
				
			}
			// Put the entry as Key with missing values in a collection int[Number of missing values] = missing values
			attributecollection.put(o, attributeLocation);
			logger.log(Level.FINEST, "On entry: " + o.intValue() + " Found Missing attributes on following locations: " + arrayRepresentation(attributeLocation));
		}
	}

	private static String[] readAttributes() {
		
		// Size of DataSet Columns
		String[] attributes	= new String[magicNumber];
		
		// Read the attribute names for convenience
		try{
			BufferedReader bufRdr = new BufferedReader(new FileReader(new File("Attributes.txt")));
			String line = bufRdr.readLine();
			int count = 0;
			
			while ((line = bufRdr.readLine()) != null) {
				attributes[count++] = line;
			}
			bufRdr.close();
		}catch(Exception e)
		{
			logger.log(Level.SEVERE, "File not found or cannot be read.");
		}
		
		return attributes;
	}
	
	
	private static String arrayRepresentation(int[] attributeLocation){
		
		String arrayToString = Arrays.toString(attributeLocation);
		
		arrayToString = arrayToString.replaceAll("\\s0,", "");
		
		arrayToString = arrayToString.replaceAll("0]", "]").trim();
		arrayToString = arrayToString.replaceAll(",\\s]", "]").trim();
		
		return arrayToString;
		
		
	}
}
