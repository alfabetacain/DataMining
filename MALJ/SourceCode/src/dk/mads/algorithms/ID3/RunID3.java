package dk.mads.algorithms.ID3;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import dk.mads.data.Class_Label;
import dk.mads.data.Response;

public class RunID3 {

	private static final Logger logger =
	        Logger.getLogger(RunID3.class.getName());
	/**
	 * @param args
	 */
	public static void run(ArrayList<Response> response) {
		
		logger.log(Level.FINE, "DataManager loaded "+response.size() + " Responses");

		ID3 brancher = new ID3();
		response.get(0);
		Node node = brancher.buildTree(response, Response.getAttributeList(), (data,attributes) -> {
			
			double infoD = infoD(data);
			
			List<Container> attributeGains = new ArrayList<>();
			for (Object att : attributes) {
				double infoA = 0.0;
				for (Object att1 : ID3.getAllPossibleValues(att)) {
					List<Response> myShare = data.stream().filter(resp -> resp.getAttributeValue(att) == att1).collect(Collectors.toList());
					infoA += myShare.size() / response.size() * infoD(myShare);
				}
				Container cont = new Container();
				cont.attribute = att;
				cont.gain = infoD - infoA;
				attributeGains.add(cont);
			}
			Collections.sort(attributeGains);
			return attributeGains.get(attributeGains.size()-1).attribute;
			
		});
		logger.log(Level.INFO, node + " Size: " + node.depth());
	}
	
	
	public static double infoD(List<Response> data) {
		double infoD = 0.0;
		for (Class_Label value : Class_Label.values()) {
			double matches = data.stream().filter(resp -> resp.label == value).count();
			infoD -= matches / data.size() * (Math.log(matches / data.size()) / Math.log(2));
		}
		return infoD;
	}

}
