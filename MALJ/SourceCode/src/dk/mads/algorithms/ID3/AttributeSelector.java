package dk.mads.algorithms.ID3;

import java.util.List;

import dk.mads.data.Response;

public interface AttributeSelector {
	public Object select(List<Response> data, List<Object> attributes);
}
