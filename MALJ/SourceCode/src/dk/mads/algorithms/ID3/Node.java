package dk.mads.algorithms.ID3;
import java.util.ArrayList;
import java.util.List;

import dk.mads.data.Class_Label;


public class Node {
	@SuppressWarnings("rawtypes")
	public List<Branch> branches = new ArrayList<>();
	public Class_Label label;
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		if (branches.size() == 0) {
			builder.append("\r\nLeaf node -> Class: ");
			builder.append(label.toString());
		}
		else {
			builder.append("Not leaf node");
			for (@SuppressWarnings("rawtypes") Branch b : branches) {
				builder.append("\r\nBranch -> Condition = " + b.getCondition() + " -> " + b.getNode().toString());
			}
		}
		return builder.toString();
	}
	
	public int depth() {
		int depth = 1;
		int max = 0;
		for (@SuppressWarnings("rawtypes") Branch b : branches) {
			int newDepth = 0;
			if (b.getNode() != null)
				newDepth = b.getNode().depth();
			if (newDepth > max)
				max = newDepth;
		}
		return depth + max;
	}
}
