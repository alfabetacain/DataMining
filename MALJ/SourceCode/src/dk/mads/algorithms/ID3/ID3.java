package dk.mads.algorithms.ID3;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import dk.mads.data.Class_Label;
import dk.mads.data.Degree;
import dk.mads.data.FrequenceGamesPlayed;
import dk.mads.data.Interestingness;
import dk.mads.data.PhoneOS;
import dk.mads.data.Response;


public class ID3 {
	
	private static final Logger logger =
	        Logger.getLogger(ID3.class.getName());

	
	public Node buildTree(List<Response> data, List<Object> objs, AttributeSelector selector) {
		Node node = new Node();
		if (data.stream().allMatch(resp -> resp.label == dk.mads.data.Class_Label.FEMALE)) {
			logger.log(Level.FINE, "All identical, returning node...");
			node.label = dk.mads.data.Class_Label.FEMALE;
			return node;
		}
		if (data.stream().allMatch(resp -> resp.label == dk.mads.data.Class_Label.MALE)) {
			logger.log(Level.FINE, "All identical, returning node...");
			node.label = dk.mads.data.Class_Label.MALE;
			return node;
		}
		
		if (objs.isEmpty()) {
			logger.log(Level.FINE, "No more attribute");
			//majority vote
			node.label = data.stream().filter(resp -> resp.label == dk.mads.data.Class_Label.FEMALE).count() > data.size() / 2 ? dk.mads.data.Class_Label.FEMALE : dk.mads.data.Class_Label.MALE;
			return node;
		}
		Object att = selector.select(data, objs);
		List<Object> filteredAttributes = objs.stream().filter(objo -> objo != att).collect(Collectors.toList());
		for (Object obj : getAllPossibleValues(att)) {
			List<Response> branchData = (List<Response>) data.stream().filter(resp -> resp.getAttributeValue(att) == obj).collect(Collectors.toList());
			if (branchData.isEmpty()) {
				Node leafNode = new Node();
				leafNode.label = node.label = data.stream().filter(resp -> resp.label == dk.mads.data.Class_Label.FEMALE).count() > data.size() / 2 ? dk.mads.data.Class_Label.FEMALE : dk.mads.data.Class_Label.MALE;
				node.branches.add(new Branch<Object>(obj, leafNode));
			}
			else
				node.branches.add(new Branch<Object>(obj, buildTree(branchData, filteredAttributes, selector)));
		}
		return node;
			
	}
	
	public static Object[] getAllPossibleValues(Object obj) {

		if(obj.equals(Class_Label.class))
		{
			return Class_Label.values();
		}
		if(obj.equals(PhoneOS.class))
		{
			return PhoneOS.values();
		}
		if(obj.equals(FrequenceGamesPlayed.class))
		{
			return FrequenceGamesPlayed.values();
		}
		if(obj.equals(Interestingness.class))
		{
			return Interestingness.values();
		}
		if(obj.equals(Degree.class))
		{
			return Degree.values();
		}
		
		return null; //If we get down here something is wrong;
	}
}
