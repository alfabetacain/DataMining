package dk.mads.algorithms.ID3;
public class Container implements Comparable<Container> {
	public Object attribute;
	public double gain;
	@Override
	public int compareTo(Container o) {

		if (gain < o.gain)
			return -1;
		if (gain > o.gain)
			return 1;
		return 0;
	}
	
	
}
