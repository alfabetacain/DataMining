package dk.mads.algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import dk.mads.data.Class_Label;
import dk.mads.data.Degree;
import dk.mads.data.FrequenceGamesPlayed;
import dk.mads.data.Game;
import dk.mads.data.Gender;
import dk.mads.data.Interestingness;
import dk.mads.data.PhoneOS;
import dk.mads.data.Response;



public class MapObject {
	
	private static final Logger logger =
			Logger.getLogger(MapObject.class.getName());
	
	public static ArrayList<Response> mapToObject(Map<String, HashMap<Integer,String[]>> data)
	{
		logger.log(Level.INFO, "Begin Mapping To Objects");
		
		// Result array for mapped response objects.
		ArrayList<Response> responses = new ArrayList<>();
		
		// Fetch data to map.
		Set<Integer> entries = data.get("goodset").keySet();
		
		for ( Integer key : entries )
		{
			Response response = new Response();
			
			response.Timestamp = data.get("goodset").get(key)[0];
			response.Age = Integer.parseInt(data.get("goodset").get(key)[1]);
			response.Gender = fetchGender(data.get("goodset").get(key)[2]);
			
			// TODO Move this to cleaning.
			String shoesize = data.get("goodset").get(key)[3].replaceAll("\"", "").replaceAll("\\s", "").replaceAll(",", ".");
			
			response.Shoesize = Double.parseDouble(shoesize);
			
			// TODO Move this to cleaning.
			String temp = data.get("goodset").get(key)[4].replaceAll("\"", "").replaceAll(",", "").trim();
			
			if (temp.length() > 2)
			{
				temp = temp.substring(0, 3);
			}

			
			response.Height = Integer.parseInt(temp);
			// Data kept for future. But will crash if out commented. TODO Handle.
//			response.SeatPlacement = Integer.parseInt(data.get("goodset").get(key)[5]);
//			response.RowPlacement = data.get("goodset").get(key)[6];
			
			response.ProgrammingLangaugesKnown = data.get("goodset").get(key)[7].split(";");
			response.PrefferedPhoneOS = fetchPhoneOS(data.get("goodset").get(key)[8]);			
			response.InterestInDesignEfficientDatabases = fetchInterest(data.get("goodset").get(key)[9]);
			response.InterestInCreatePredictiveModels = fetchInterest(data.get("goodset").get(key)[10]);
			response.InterestInDefineGroupsOfSimilarObjects = fetchInterest(data.get("goodset").get(key)[11]);
			response.InterestInVisualiseData = fetchInterest(data.get("goodset").get(key)[12]);
			response.InterestInStudyPatternsOnSets = fetchInterest(data.get("goodset").get(key)[13]);
			response.InterestInStudyPatternsOnSequences = fetchInterest(data.get("goodset").get(key)[14]);
			response.InterestInStudyPatternsOnGraphs = fetchInterest(data.get("goodset").get(key)[15]);
			response.InterestInStudyPatternsOnText = fetchInterest(data.get("goodset").get(key)[16]);
			response.InterestInStudyPatternsOnImages = fetchInterest(data.get("goodset").get(key)[17]);
			response.InterestInCodeDataminingAlgorithms = fetchInterest(data.get("goodset").get(key)[18]);
			response.InterestInUseOfftheshelfDataminingTools = fetchInterest(data.get("goodset").get(key)[19]);
			response.Howoftendoyouplayvideogames = fetchFrequency(data.get("goodset").get(key)[20]);
			
			response.GamesPlayed = fetchGamesPlayed(data.get("goodset").get(key)[21]);
			
			// Data kept for future. But will crash if out commented. TODO Handle.
//			response.HowdoyoucommutetoITU = data.get("goodset").get(key)[22];
//			public String[] HowdoyoucommutetoITU;
//			response.HowdoyoucommutetoITU = data.get("goodset").get(key)[23];
//			response.HowdoyoucommutetoITU = data.get("goodset").get(key)[24];
//			response.HowdoyoucommutetoITU = data.get("goodset").get(key)[25];
//			response.HowdoyoucommutetoITU = data.get("goodset").get(key)[26];
//			response.HowdoyoucommutetoITU = data.get("goodset").get(key)[27];
//			response.HowdoyoucommutetoITU = data.get("goodset").get(key)[28];
//			response.HowdoyoucommutetoITU = data.get("goodset").get(key)[29];
//			response.HowdoyoucommutetoITU = data.get("goodset").get(key)[30];
//			response.HowdoyoucommutetoITU = data.get("goodset").get(key)[31];
//			response.HowdoyoucommutetoITU = data.get("goodset").get(key)[32];
//			response.HowdoyoucommutetoITU = data.get("goodset").get(key)[33];
//			response.HowdoyoucommutetoITU = data.get("goodset").get(key)[34];
//			response.RandomNumbers1to10 = dat.get("goodset").get(key)[35];
			
			
			response.HowtallareyoucomparedtoHéctor = data.get("goodset").get(key)[36];
			response.pizzapreferredtunacapersolives = Integer.parseInt(data.get("goodset").get(key)[37]);
			response.pizzapreferredhampineapple = Integer.parseInt(data.get("goodset").get(key)[38]);
			response.pizzapreferredmushroomsolivesonionspepper = Integer.parseInt(data.get("goodset").get(key)[39]);
			response.pizzapreferredeggplantpepperonion = Integer.parseInt(data.get("goodset").get(key)[40]);
			response.pizzapreferredpeperonimushroomsham = Integer.parseInt(data.get("goodset").get(key)[41]);
			response.epizzapreferredpineapplemushrooms = Integer.parseInt(data.get("goodset").get(key)[42]);
			response.pizzapreferredtunapeppereggplant = Integer.parseInt(data.get("goodset").get(key)[43]);
			response.pizzapreferrednothingmargarita = Integer.parseInt(data.get("goodset").get(key)[44]);
			response.pizzapreferredpeperonitunapineappleeggplantonionspeppercapers = Integer.parseInt(data.get("goodset").get(key)[45]);
			response.Whatdegreeareyoustudying = fetchDegree(data.get("goodset").get(key)[47]);
			response.Whyareyoutakingthiscourse = data.get("goodset").get(key)[48];
			
			response.label = fecthLabel(data.get("goodset").get(key)[2]);
			response.gamesplayed = fetchGames(data.get("goodset").get(key)[21]);
			
			// Add Mapped object to object collection.
			responses.add(response);
			
		}
		
		logger.log(Level.INFO, "Done Mapping Objects");
		
		return responses;
	}
	
	// ================== Helper methods ================== \\
	
	private static Degree fetchDegree(String fetchable)
	{
		fetchable = fetchable.toUpperCase();

		switch (fetchable){
		case "GAMES-T" :
			return Degree.Games_T;
		case "SDT-SE" :
			return Degree.SDT_SE;
		case "SWU" :
			return Degree.SWU;
		case "SDT-DT" :
			return Degree.SDT_DT;
		case "SINGEL SUBJECT" :
			return Degree.UNKNOWN;
		case "GAMES-A" :
			return Degree.GAMES_A;
			default :

				return Degree.UNKNOWN;
		}
	}
	
	private static String[] fetchGames(String fetchable) {
		
		fetchable = fetchable.trim();
		String[] result = fetchable.split(",");
		
		
		return result;
	}

	private static Class_Label fecthLabel(String fetchable)
	{
		fetchable = fetchable.toUpperCase();
		
		switch (fetchable){
			case "FEMALE" :
				return Class_Label.FEMALE;
			case "MALE" :
				return Class_Label.MALE;
				default :
					return null;
		}
	}
	
	private static Game[] fetchGamesPlayed(String fetchable)
	{
		fetchable = fetchable.toUpperCase();
		String[] fetchedArray =  fetchable.split(",");
		
		Game[] gamesPlayed = new Game[fetchedArray.length];
		for (int i = 0 ; i < fetchedArray.length ; i++)
		{
			switch (fetchedArray[i]){
			case "ANGRY BIRDS" :
				gamesPlayed[i] = Game.ANGRY_BIRDS;
			case "CALL OF DUTY: MODERN WARFARE" :
				gamesPlayed[i] = Game.CALL_OF_DUTY_MODERN_WARFARE;
			case "CANDY CRUSH" :
				gamesPlayed[i] = Game.CANDY_CRUSH;
			case "COUNTER STRIKE: GO" :
				gamesPlayed[i] = Game.COUNTER_STRIKE_GO;
			case "FARMVILLE" :
				gamesPlayed[i] = Game.FARMVILLE;
			case "FIFA 2014" :
				gamesPlayed[i] = Game.FIFA_2014;
			case "GRAND THEFT AUTO V" :
				gamesPlayed[i] = Game.GRAND_THEFT_AUTO_V;
			case "I HAVE NOT PLAYED ANY OF THESE GAMES" :
				gamesPlayed[i] = Game.I_HAVE_NOT_PLAYED_ANY_OF_THESE_GAMES;
			case "JOURNEY" :
				gamesPlayed[i] = Game.JOURNEY;
			case "LAST OF US" :
				gamesPlayed[i] = Game.LAST_OF_US;
			case "MINECRAFT" :
				gamesPlayed[i] = Game.MINECRAFT;
			case "STANLEY PARABLE" :
				gamesPlayed[i] = Game.STANLEY_PARABLE;
			case "WORDFEUD" :
				gamesPlayed[i] = Game.WORDFEUD;
				default :
					gamesPlayed[i] = null;
			}
		}
		return gamesPlayed;
	}
	
	private static FrequenceGamesPlayed fetchFrequency(String fetchable)
	{
		fetchable = fetchable.toUpperCase();
		
		switch (fetchable){
			case "> 20 HOURS A WEEK" :
				return FrequenceGamesPlayed.GREATERTHAN_20_HOURS_A_WEEK;
			case "< 10 HOURS A WEEK" :
				return FrequenceGamesPlayed.LESSTHAN_10_HOURS_A_WEEK;
			case "< 20 HOURS A WEEK" :
				return FrequenceGamesPlayed.LESSTHAN_20_HOURS_A_WEEK;
			case "< 5 HOURS A WEEK" :
				return FrequenceGamesPlayed.LESSTHAN_5_HOURS_A_WEEK;
			case "NEVER" : 
				return FrequenceGamesPlayed.NEVER;
			default :
				return null;
		}
	}
	
	private static Interestingness fetchInterest(String fetchable)
	{
		fetchable = fetchable.toUpperCase();
		
		switch (fetchable){
			case "MEH" :
				return Interestingness.MEH;
			case "NOT INTERESTED" :
				return Interestingness.NOT_INTERESTED;
			case "SOUNDS INTERESTING" :
				return Interestingness.SOUNDS_INTERESTING;
			case "VERY INTERESTED" :
				return Interestingness.VERY_INTERESTED;		
			default :
				return null;
		}
	}
	
	private static PhoneOS fetchPhoneOS(String fetchable) 
	{
		fetchable = fetchable.toUpperCase();
		
		switch (fetchable){
			case "WINDOWS" :
				return PhoneOS.WINDOWS;
			case "ANDROID" :
				return PhoneOS.ANDROID;	
			case "IOS" :
				return PhoneOS.IOS;
			case "OSX" :
				return PhoneOS.OSX;
			default :
				return null;
		}
	}
	
	private static Gender fetchGender(String fetchable)
	{
		fetchable = fetchable.toUpperCase();
		
		switch (fetchable){
			case "FEMALE" :
				return Gender.FEMALE;
			case "MALE" :
				return Gender.MALE;
				default :
					return null;
		}
	}
}
