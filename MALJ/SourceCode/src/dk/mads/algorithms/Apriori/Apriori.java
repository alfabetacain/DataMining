package dk.mads.algorithms.Apriori;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Apriori {
	
	private static final Logger logger =
			Logger.getLogger(Apriori.class.getName());

	
    public static void run(String[][] gamesPlayed) {
        // TODO: Select a reasonable support threshold.
        final int supportThreshold = 10;
        apriori( gamesPlayed, supportThreshold );
    }

    public static List<ItemSet> apriori( String[][] transactions, int supportThreshold ) {

        Hashtable<ItemSet, Integer> frequentItemSets = generateFrequentItemSetsLevel1( transactions, supportThreshold );
        List<Hashtable<ItemSet,Integer>> complete = new ArrayList<>();
        complete.add(frequentItemSets);
        for (int k = 1; frequentItemSets.size() > 0; k++) 
        {
        	logger.log(Level.FINEST, "Finding frequent itemsets of length " + (k + 1));

        	logger.log(Level.FINEST, "SupportThreshold: " + supportThreshold + "; FrequentItemSets: " + frequentItemSets);

            frequentItemSets = generateFrequentItemSets( supportThreshold, transactions, frequentItemSets );
            // Add Frequent Item Sets to list.
            complete.add(frequentItemSets);
            logger.log(Level.FINEST, "Found: " + frequentItemSets.size());
        }

        // Create association rules from the frequent itemsets
        for (int i = complete.size()-1; i >= 0; i--)
        {
        	Hashtable<ItemSet,Integer> table = complete.get(i);

        	for(ItemSet set : table.keySet())
        	{
        		for ( int k = i-1; k >= 0 ; k--)
        		{
        			Hashtable<ItemSet,Integer> table2 = complete.get(k);
        			for( ItemSet set2 : table2.keySet()) 
        			{
        				if( contains(set2.set,set.set))
        				{
							double setSupp = table.get(set);
							double set2Supp = table2.get(set2);
							double implies = setSupp/set2Supp;
							System.out.println("Support -> " + set + "->" + setSupp);
							System.out.println("Support -> " + set2 + "->" + set2Supp);
        					System.out.println(set2 + "->" + set + "=" + implies);
        				}
        			}
        		}
        	}
        }


        // TODO: return something useful DID NOT FINISH DUE TO TIME LIMIT.
        return null;
    }

    private static Hashtable<ItemSet, Integer> generateFrequentItemSets( int supportThreshold, String[][] transactions,
                    Hashtable<ItemSet, Integer> lowerLevelItemSets ) {

        // Generate candidate itemsets from the lower level itemsets - join step
		Hashtable<ItemSet,Integer> table = new Hashtable<>();
		int size = -1;

		for(ItemSet set: lowerLevelItemSets.keySet())
		{
			if (size == -1 )
				size = set.set.length;
			for(ItemSet set2 : lowerLevelItemSets.keySet())
			{
				if( set == set2) continue;
				ItemSet newset = joinSets(set,set2);
				if (newset != null)
					table.put(newset,0);
			}
		}

    	// Check if all subsets of the candidate item sets meet the apriori property - prune step    	
        Enumeration<ItemSet> iter = table.keys();

      	while(iter.hasMoreElements() )
      	{
      		ItemSet set = iter.nextElement();
      		if ( !prune(set, lowerLevelItemSets)) table.remove(set);
      	}

		/*
         * Check the support for the candidates left after pruning and add only those
         * that have enough support (i.e. support >= support_threshold) to the set.
         */
		iter = table.keys();
		while (iter.hasMoreElements())
		{
			ItemSet set = iter.nextElement();
			int support = countSupport(set.set,transactions);
			if ( support > supportThreshold)
			{
				table.put(set,support);
			}
			else
			{
				table.remove(set);
			}
		}
        // The Frequent Set.
        return table;
    }
	
	private static boolean prune(ItemSet candidate, Hashtable<ItemSet,Integer> table)
	{
			int size = candidate.set.length-1;
			int start = 0 ;
      		String[] innerSet = candidate.set;
      		String[] subSet = new String[size];

      		while (start + size < innerSet.length)
      		{

      			System.arraycopy(innerSet, start++, subSet, 0, size);

      			ItemSet newset = new ItemSet(subSet);

      			if (!table.containsKey(newset))
      			{
      				logger.log(Level.FINER, "Pruning " + candidate + " because of " + newset + "...");
      				return false;
      			}

      			if ( table.get(newset) == 0)
      			{
      				return false;
      			}
      		}
      		return true;
	}

    private static ItemSet joinSets( ItemSet first, ItemSet second ) {

		if(first.set.length != second.set.length) return null;
		
		int length = first.set.length;
		
		for(int i = 0; i < first.set.length-1; i++)
			if(first.set[i] != second.set[i]) return null;
		
		String[] newset = new String[first.set.length+1];
		System.arraycopy(first.set, 0, newset, 0, first.set.length-1);
		
		if(  first.set[first.set.length-1].compareTo(second.set[second.set.length-1]) < 0) {
			newset[newset.length-2] = first.set[length-1];
			newset[newset.length-1] = second.set[length-1];
		}
		else
		{
			newset[newset.length-2] = second.set[length-1];
			newset[newset.length-1] = first.set[length-1];
		}
		
		return new ItemSet(newset);
    }

    private static Hashtable<ItemSet, Integer> generateFrequentItemSetsLevel1( String[][] transactions, int supportThreshold ) {

		Hashtable<ItemSet,Integer> table = new Hashtable<>();
		
		for(int i = 0; i<transactions.length; i++)
		{
			for(int k = 0; k<transactions[i].length; k++)
			{
				ItemSet set = new ItemSet( new String[]{transactions[i][k]});
				if( table.containsKey(set)) table.put(set,table.get(set)+1);
				else table.put(set,1);
			}
		}

		Enumeration<ItemSet> enumerator = table.keys();
		while ( enumerator.hasMoreElements())
		{
			ItemSet set = enumerator.nextElement();
			if(table.get(set) < supportThreshold) table.remove(set);
		}
        return table;
    }

    private static int countSupport( String[] itemSet, String[][] transactions ) {
        // Assumes that items in ItemSets and transactions are both unique
		int count = 0;
		for (int i = 0; i < transactions.length; i++)
		{
			if( contains(itemSet, transactions[i])) count++;
		}
        // Return Support Counted.
        return count;
    }
	
	private static boolean contains(String[] containee, String[] container)
	{
		for (int i = 0; i < containee.length; i++)
		{
			boolean found = false;
			for(int k = 0; k < container.length; k++)
			{
				if(containee[i].equals(container[k])) found = true;
			}
			if(!found) return false;
		}
		return true;
	}

}
