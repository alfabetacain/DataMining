package dk.mads.algorithms.Apriori;

/***
 * The ItemSet class is used to store information concerning a single transaction.
 * @author Mads Ljungberg : Malj@itu.dk
 *
 */
public class ItemSet {
	
    final String[] set;

    /***
     * Creates a new instance of the ItemSet class.
     * @param set Transaction content
     */
    public ItemSet( String[] set ) {
        this.set = set;
    }

    @Override
    /**
     * hashCode functioned used internally in Hashtable
     */
    public int hashCode() {
        String code = "";
        for (int i = 0; i < set.length; i++) {
            code += set[i];
        }
        return code.hashCode();
    }

    
    @Override
    /**
     * Used to determine whether two ItemSet objects are equal
     */
    public boolean equals( Object o ) {
        if (!(o instanceof ItemSet)) {
            return false;
        }
        ItemSet other = (ItemSet) o;
        if (other.set.length != this.set.length) {
            return false;
        }
        for (int i = 0; i < set.length; i++) {
            if (!set[i].equals(other.set[i])) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
		boolean isFirst = true;
        for (String i : set)
        {
			if (!isFirst) {
				sb.append(",");
			}
			else
				isFirst = false;
            sb.append(i);
        }
        sb.append("]");
        return sb.toString();
    }
}
