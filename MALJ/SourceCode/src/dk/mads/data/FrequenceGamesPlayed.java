package dk.mads.data;

public enum FrequenceGamesPlayed {
	NEVER,LESSTHAN_5_HOURS_A_WEEK, LESSTHAN_10_HOURS_A_WEEK, LESSTHAN_20_HOURS_A_WEEK , GREATERTHAN_20_HOURS_A_WEEK
}
