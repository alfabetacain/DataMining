package dk.mads.data;

import java.util.ArrayList;

public class Response {
	
	public static ArrayList<Object> getAttributeList()
	{
		ArrayList<Object> attributes = new ArrayList<Object>();
		attributes.add(Interestingness.class);
//		attributes.add(Gender.class);
		attributes.add(PhoneOS.class);
		attributes.add(FrequenceGamesPlayed.class);
		attributes.add(Degree.class);
		
		
		return attributes;
	}
	

	public Class_Label label;
	
	public String Timestamp;
	
	// K-mean candidates
	public int Age;
	public Gender Gender;
	public double Shoesize;
	public int Height;
	
	// In consideration 
	public int SeatPlacement;
	public String RowPlacement;
	public String[] ProgrammingLangaugesKnown;
	public PhoneOS PrefferedPhoneOS;
	
	// ID3 
	public Interestingness InterestInDesignEfficientDatabases;
	public Interestingness InterestInCreatePredictiveModels;
	public Interestingness InterestInDefineGroupsOfSimilarObjects;
	public Interestingness InterestInVisualiseData;
	public Interestingness InterestInStudyPatternsOnSets;
	public Interestingness InterestInStudyPatternsOnSequences;
	public Interestingness InterestInStudyPatternsOnGraphs;
	public Interestingness InterestInStudyPatternsOnText;
	public Interestingness InterestInStudyPatternsOnImages;
	public Interestingness InterestInCodeDataminingAlgorithms;
	public Interestingness InterestInUseOfftheshelfDataminingTools;
	
	public FrequenceGamesPlayed Howoftendoyouplayvideogames;
	
	// Apriori candidate
	public Game[] GamesPlayed;
	public String[] gamesplayed;
	
	public String[] HowdoyoucommutetoITU;
	
	// Might squeeze it into a sequence.
	public String[][] traversalSequence;
	
	//could be ID 3
	public String ITUlocations1checkpoint;
	public String ITUlocations3checkpoint;
	public String ITUlocations4checkpoint;
	public String ITUlocations2checkpoint;
	public String ITUlocations5checkpoint;
	public String ITUlocations6checkpoint;
	public String ITUlocations7checkpoint;
	public String ITUlocations8checkpoint;
	public String ITUlocations9checkpoint;
	public String ITUlocations10checkpoint;
	public String ITUlocations11checkpoint;
	public String ITUlocations12checkpoint;
	
	public String[] RandomNumbers1to10;
	
	// ID 3
	public String HowtallareyoucomparedtoHéctor;
	
	public String[][] pizzaRateSequence;
	public int pizzapreferredpeperoniolives;
	public int pizzapreferredtunacapersolives;
	public int pizzapreferredhampineapple;
	public int pizzapreferredmushroomsolivesonionspepper;
	public int pizzapreferredeggplantpepperonion;
	public int pizzapreferredpeperonimushroomsham;
	public int epizzapreferredpineapplemushrooms;
	public int pizzapreferredtunapeppereggplant;
	public int pizzapreferrednothingmargarita;
	public int pizzapreferredpeperonitunapineappleeggplantonionspeppercapers;
	
	public Degree Whatdegreeareyoustudying;
	public String Whyareyoutakingthiscourse;
	
	public Object getAttributeValue(Object Attribute)
	{
		
		if(Attribute.equals(Class_Label.class))
		{
			return this.label;
		}
		if(Attribute.equals(Interestingness.class))
		{
			// How to handle this ?
			
		}
		if(Attribute.equals(FrequenceGamesPlayed.class))
		{
			return this.Howoftendoyouplayvideogames;
		}
		if(Attribute.equals(PhoneOS.class))
		{
			return this.PrefferedPhoneOS;
		}
		if(Attribute.equals(Degree.class))
		{
			return this.Whatdegreeareyoustudying;
		}
		
//		if (Attribute.equals(Gender.class)))
//		{
//			return this.gender; 
//		}
		
		return null; //If we get down here something is wrong;
	}
}
