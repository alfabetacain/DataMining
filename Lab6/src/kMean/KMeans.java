package kMean;
import java.util.*;

import data.*;


public class KMeans {

	public static ArrayList<KMeanCluster> KMeansPartition(int k, ArrayList<Iris> data)
	{
		ArrayList<KMeanCluster> clusters = new ArrayList<>(k);
		
		for (int i = 0; i < k; i++)
		{
			KMeanCluster cluster = new KMeanCluster();
			cluster.ClusterMembers.add(data.get(i));
			clusters.add(cluster);
		}
		
		boolean notChanged = false;
		
		while(!notChanged)
		{
			notChanged = true;
			HashMap<KMeanCluster,Iris> means = new HashMap<>();
			for(KMeanCluster cluster : clusters) 
				means.put(cluster,calculateMean(cluster.ClusterMembers));
			for(Iris iris : data)
			{
				double shortestDistance = Double.MAX_VALUE;
				KMeanCluster shortestCluster = null;
				
				for(KMeanCluster cluster : clusters)
				{
					double dist = distance(iris,means.get(cluster));
					
					if(dist < shortestDistance)
					{
						shortestDistance = dist;
						shortestCluster = cluster;
					}
				}
				if (!shortestCluster.ClusterMembers.contains(iris))
				{
					shortestCluster.ClusterMembers.add(iris);
					notChanged = false;
				}
			}
		}
		
		return clusters;
		
	}
	
	private static double distance(Iris iris1, Iris iris2)
	{
		double dist = Math.pow(iris1.Sepal_Length - iris2.Sepal_Length,2);
		dist += Math.pow(iris1.Sepal_Width - iris2.Sepal_Width,2);
		dist += Math.pow(iris1.Petal_Length - iris2.Petal_Length,2);
		dist += Math.pow(iris1.Petal_Width - iris2.Petal_Width,2);
		
		return Math.sqrt(dist);
	}
	
	private static Iris calculateMean(ArrayList<Iris> data)
	{
		float Sepal_Length;
		float Sepal_Width;
		float Petal_Length;
		float Petal_Width;
		
		Sepal_Length = Sepal_Width = Petal_Length = Petal_Width = 0.0f;
		
		for(Iris iris : data)
		{
			Sepal_Length += iris.Sepal_Length;
			Sepal_Width += iris.Sepal_Width;
			Petal_Length += iris.Petal_Length;
			Petal_Width += iris.Petal_Width;
		}
		int count = data.size();
		Iris iris = new Iris(Sepal_Length/count, Sepal_Width/count,Petal_Length/count,Petal_Width/count,"Iris-setosa");
		
		return iris;
	}
}
