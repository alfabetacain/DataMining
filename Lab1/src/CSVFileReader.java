

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * The CSVFileReader class is used to load a csv file
 * @author andershh and hjab
 *
 */
public class CSVFileReader {
	/**
	 * The read method reads in a csv file as a two dimensional string array.
	 * This method is utilizes the string.split method for splitting each line of the data file.
	 * String tokenizer bug fix provided by Martin Marcher.
	 * @param csvFile File to load
	 * @param seperationChar Character used to seperate entries
	 * @param nullValue What to insert in case of missing values
	 * @return Data file content as a 2D string array
	 * @throws IOException
	 */
	public static String[][] readDataFile(String csvFile, String seperationChar, String nullValue, boolean checkForQuoteMarkRange) throws IOException
	{
		List<String[]> lines = new ArrayList<String[]>();
		BufferedReader bufRdr = new BufferedReader(new FileReader(new File(csvFile)));
		
		// read the header
		String line = bufRdr.readLine();
		
		while ((line = bufRdr.readLine()) != null) {
			String[] arr = line.split(seperationChar); 
			
			for(int i = 0; i < arr.length; i++)
			{
				if(arr[i].equals(""))
				{
					arr[i] = nullValue;
				}				
			}
			
			if(checkForQuoteMarkRange)
			{
				arr = CheckForQuoteMarkRange(arr);
			}

			lines.add(arr);	
		}
		
		String[][] ret = new String[lines.size()][];
		bufRdr.close();
		return lines.toArray(ret);
	}
	
	/**
	 * This method checks a line of date from the data to ensure that no piece of the data
	 * has been split up due to how the questionnaire data set handles decimal values and lists of data - eg. "181,5" and "Fifa 2014, Grand theft auto V".
	 * This is a problem since we use the comma to split read-in lines of data into attributes of data,
	 * and since because of this attribute data may erroneously be split up e.g. "181,5" is split up into two attributes - 181 and 5.
	 * @param line Line of data to check.
	 * @return Line of data corrected if needed.
	 */
	private static String[] CheckForQuoteMarkRange(String[] line)
	{
		ArrayList<String> result = new ArrayList<String>();
		ArrayList<String> keeper = new ArrayList<String>();
		boolean addToKeeper = false;
		
		for(String part : line)
		{
			if(part.contains("\""))
			{
				if(addToKeeper)
				{
					addToKeeper = false;
					keeper.add(part);					
					String toAdd = keeper.toString();
					result.add(toAdd.substring(1, toAdd.length()-1));//Done to remove brackets from the string.
					keeper.clear();
				}
				else
				{
					addToKeeper = true;
					keeper.add(part);
				}
			}
			else
			{
				if(addToKeeper)
				{
					keeper.add(part);
				}
				else
				{
					result.add(part);
				}
			}
		}
		String[] res = new String[result.size()];
		
		return result.toArray(res);
	}
	
	private static boolean tryParseInt(String value)  
	{  
	     try  
	     {  
	         Integer.parseInt(value);  
	         return true;  
	      } catch(NumberFormatException nfe)  
	      {  
	          return false;  
	      }  
	}
	
	public static void main(String args[]) {
		try {
			Map<Integer,Integer> missing = new HashMap<>();
			String[][] data = readDataFile("DataMining2015Responses.csv",",", "-",true);
			Map<Integer,String> mapping = new HashMap<>();
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("Attributes.txt"), "UTF-8"));
				String line;
				int count = 0;
				while ((line = reader.readLine()) != null) 
					mapping.put(count++, line);
				BufferedWriter writer = new BufferedWriter(new FileWriter("Data.java"));
				writer.write("public class Data { ");
				for (int i : mapping.keySet())
					writer.write("public String " + mapping.get(i).replaceAll("[-\\.\\s,\\(\\)\\?/\"\\[\\]]", "")+ ";\r\n");
				writer.write(" }");
				writer.flush();
				writer.close();
			} catch (Exception e) {
				System.out.println(e);
			}
			Data[] data2 = new Data[data.length-1];
			for (int k = 1; k < data.length; k++) {
				String[] array = data[k];
				for (int j = 0; j < array.length; j++)
					array[j] = array[j].trim();
				Data object = new Data();
				object.Timestamp = array[0];
				object.Age = Integer.parseInt(array[1]);
				object.Gender = array[2].equalsIgnoreCase("Male");
				object.Shoesize = Double.parseDouble(array[3].replaceAll(" ", "").replaceAll("\"", "").replaceAll(",", "."));
				object.Height = Integer.parseInt(array[4]);
				object.Whichseatareyousittingdidyousitonduringtheintroductionlecture = Pattern.compile("\\D").matcher(array[5]).find() ? -1 : Integer.parseInt(array[5]);
				object.Whichrowareyousittingdidyousitinduringtheintroductionlecture = array[6];
				String[] tempArray = array[7].split("[,;]");
				List<String> ls = new ArrayList<>();
				for (int x = 0; x < tempArray.length; x++) {
					String language = tempArray[x].replaceAll("\"", "").trim().toUpperCase();
					if (!language.isEmpty())
						ls.add(language);
				}
				object.Whichprogramminglanguagesdoyouknow = ls.toArray(new String[ls.size()]);
				object.WhichphoneOSdoyouprefer = array[8];
				object.WhichtopicswouldyouprefertolearninthiscourseDesignefficientdatabasesforlargeamountsofdata = Interestingness.valueOf(array[9].toUpperCase().replaceAll(" ", "_"));
				object.WhichtopicswouldyouprefertolearninthiscourseCreatepredictivemodelsegweatherorstockmarketprediction = Interestingness.valueOf(array[10].toUpperCase().replaceAll(" ", "_"));
				object.WhichtopicswouldyouprefertolearninthiscourseDefinegroupsofsimilarobjectsegusersfromadatingsite = Interestingness.valueOf(array[11].toUpperCase().replaceAll(" ", "_"));
				object.WhichtopicswouldyouprefertolearninthiscourseVisualisedata = Interestingness.valueOf(array[12].toUpperCase().replaceAll(" ", "_"));
				object.WhichtopicswouldyouprefertolearninthiscourseStudypatternsonsetsegAmazonshoppingcarts = Interestingness.valueOf(array[13].toUpperCase().replaceAll(" ", "_"));
				object.WhichtopicswouldyouprefertolearninthiscourseStudypatternsonsequencesegexercisesonaworkoutsession = Interestingness.valueOf(array[14].toUpperCase().replaceAll(" ", "_"));
				object.WhichtopicswouldyouprefertolearninthiscourseStudypatternsongraphsegFacebook = Interestingness.valueOf(array[15].toUpperCase().replaceAll(" ", "_"));
				object.WhichtopicswouldyouprefertolearninthiscourseStudypatternsontextegspammail = Interestingness.valueOf(array[16].toUpperCase().replaceAll(" ", "_"));
				object.WhichtopicswouldyouprefertolearninthiscourseStudypatternsonimagesegfacedetection = Interestingness.valueOf(array[17].toUpperCase().replaceAll(" ", "_"));
				object.WhichtopicswouldyouprefertolearninthiscourseCodedataminingalgorithms = Interestingness.valueOf(array[18].toUpperCase().replaceAll(" ", "_"));
				object.WhichtopicswouldyouprefertolearninthiscourseUseofftheshelfdataminingtools = Interestingness.valueOf(array[19].toUpperCase().replaceAll(" ", "_"));
				object.Howoftendoyouplayvideogames = array[20];
				
				//games played
				tempArray = array[21].split(",");
				List<Game> games = new ArrayList<>();
				for (String s : tempArray) {
					s = s.trim().replaceAll("[\\s:]+", "_").replaceAll("\"", "").toUpperCase();
					games.add(Game.valueOf(s));
				}
				object.Whichofthesegameshaveyouplayed = games.toArray(new Game[games.size()]);
				object.HowdoyoucommutetoITU = array[22].split(",");
				object.InwhichorderdoyounormallytraversetheseITUlocations1checkpoint = array[23];
				object.InwhichorderdoyounormallytraversetheseITUlocations2checkpoint =  array[24];
				object.InwhichorderdoyounormallytraversetheseITUlocations3checkpoint =  array[25];
				object.InwhichorderdoyounormallytraversetheseITUlocations4checkpoint =  array[26];
				object.InwhichorderdoyounormallytraversetheseITUlocations5checkpoint =  array[27];
				object.InwhichorderdoyounormallytraversetheseITUlocations6checkpoint =  array[28];
				object.InwhichorderdoyounormallytraversetheseITUlocations7checkpoint =  array[29];
				object.InwhichorderdoyounormallytraversetheseITUlocations8checkpoint =  array[30];
				object.InwhichorderdoyounormallytraversetheseITUlocations9checkpoint =  array[31];
				object.InwhichorderdoyounormallytraversetheseITUlocations10checkpoint=  array[32];
				object.InwhichorderdoyounormallytraversetheseITUlocations11checkpoint=  array[33];
				object.InwhichorderdoyounormallytraversetheseITUlocations12checkpoint=  array[34];
				
				//normalizing random numbers
				tempArray = array[35].split("[;,]");
				int[] randoms = new int[3];
				for (int x = 0; x < tempArray.length; x++)
				{
					String number = tempArray[x].replaceAll("\\D", "").trim();
					if (number.isEmpty())
						randoms[x] = -1;
					else 
						randoms[x] = Integer.parseInt(number);
				}
				if (tempArray.length == 1)
				{
					randoms[1] = -1;
					randoms[2] = -1;
				}
				if (tempArray.length == 2)
					randoms[2] = -1;
				object.Writethree3randomnumbersbetween0and10 = array[35].split(";");
				object.HowtallareyoucomparedtoHéctor = array[36];
				object.Sortthesepizzasfrommost1toleast10preferredpeperoniolives = array[37].equals("-") ? -1 : Integer.parseInt(array[37]);
				object.Sortthesepizzasfrommost1toleast10preferredtunacapersolives = array[38].equals("-") ? -1 : Integer.parseInt(array[38]);
				object.Sortthesepizzasfrommost1toleast10preferredhampineapple = array[39].equals("-") ? -1 : Integer.parseInt(array[39]);
				object.Sortthesepizzasfrommost1toleast10preferredmushroomsolivesonionspepper = array[40].equals("-") ? -1 : Integer.parseInt(array[40]);
				object.Sortthesepizzasfrommost1toleast10preferredeggplantpepperonion = array[41].equals("-") ? -1 : Integer.parseInt(array[41]);
				object.Sortthesepizzasfrommost1toleast10preferredpeperonimushroomsham = array[42].equals("-") ? -1 : Integer.parseInt(array[42]);
				object.Sortthesepizzasfrommost1toleast10preferredpineapplemushrooms = array[43].equals("-") ? -1 : Integer.parseInt(array[43]);
				object.Sortthesepizzasfrommost1toleast10preferredtunapeppereggplant = array[44].equals("-") ? -1 : Integer.parseInt(array[44]);
				object.Sortthesepizzasfrommost1toleast10preferrednothingmargarita = array[45].equals("-") ? -1 : Integer.parseInt(array[45]);
				object.Sortthesepizzasfrommost1toleast10preferredpeperonitunapineappleeggplantonionspeppercapers = array[46].equals("-") ? -1 : Integer.parseInt(array[46]);
				object.Whatdegreeareyoustudying = array[47];
				object.Whyareyoutakingthiscourse = array[48];
				data2[k-1] = object;
				for (int i = 0; i < array.length; i++) {
					if (i == 3) {
						//System.out.println(array[i]);
					}
					if (array[i].equals("-") || array[i].isEmpty())
						if (missing.get(i) != null)
							missing.put(i, missing.get(i)+1);
						else 
							missing.put(i, 1);
				}
			}
			for (Integer i : missing.keySet())
				System.out.println(i + " -> " + mapping.get(i) + " -> " + missing.get(i));
			for (Data dat : data2) {
					for (String s : dat.Whichprogramminglanguagesdoyouknow)
						System.out.println(s);
			}
			System.out.println(data[0][0]);
			System.out.println(mapping.get(0));
		} catch (IOException e) {
			System.err.println(e.getLocalizedMessage());
		}
	}
}
