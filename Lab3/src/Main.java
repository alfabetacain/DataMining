import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import data.Class_Label;

/**
 * Main class to run program from.
 * 
 * @author Anders Hartzen and Hajira Jabeen
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// First step - Load data and convert to Mushroom objects.
		ArrayList<Mushroom> mushrooms = DataManager.LoadData();
		System.out.println("DataManager loaded "+mushrooms.size() + " mushrooms");
		ID3 brancher = new ID3();
		Node node = brancher.buildTree(mushrooms, mushrooms.get(0).getAttributeList(), (data,attributes) -> {
			double infoD = infoD(data);
			List<Container> attributeGains = new ArrayList<>();
			for (Object att : attributes) {
				double infoA = 0.0;
				for (Object att1 : ID3.getAllPossibleValues(att)) {
					List<Mushroom> myShare = data.stream().filter(mush -> mush.getAttributeValue(att) == att1).collect(Collectors.toList());
					infoA += myShare.size() / mushrooms.size() * infoD(myShare);
				}
				Container cont = new Container();
				cont.attribute = att;
				cont.gain = infoD - infoA;
				attributeGains.add(cont);
			}
			Collections.sort(attributeGains);
			return attributeGains.get(attributeGains.size()-1).attribute;
			
		});
		System.out.println(node);
		System.out.println("Size: " + node.depth());
	}
	
	
	public static double infoD(List<Mushroom> data) {
		double infoD = 0.0;
		for (Class_Label label : Class_Label.values()) {
			double matches = data.stream().filter(mush -> mush.m_Class == label).count();
			infoD -= matches / data.size() * (Math.log(matches / data.size()) / Math.log(2));
		}
		return infoD;
	}

}
