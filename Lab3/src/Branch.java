
public class Branch<T> {
	private T branchCondition;
	private Node node;
	
	public Branch(T t, Node node) {
		branchCondition = t;
		this.node = node;
	}
	
	public String getCondition() {
		return branchCondition.toString();
	}
	
	public Node getNode() {
		return node;
	}
}
