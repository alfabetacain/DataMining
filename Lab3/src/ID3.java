import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import data.Bruises;
import data.Cap_Color;
import data.Cap_Shape;
import data.Cap_Surface;
import data.Class_Label;
import data.Gill_Attachment;
import data.Gill_Color;
import data.Gill_Size;
import data.Gill_Spacing;
import data.Habitat;
import data.Odor;
import data.Population;
import data.Ring_Number;
import data.Ring_Type;
import data.Spore_Print_Color;
import data.Stalk_Color_Above_Ring;
import data.Stalk_Color_Below_Ring;
import data.Stalk_Root;
import data.Stalk_Shape;
import data.Stalk_Surface_Above_Ring;
import data.Stalk_Surface_Below_Ring;
import data.Veil_Color;
import data.Veil_Type;


public class ID3 {
	
	public Node buildTree(List<Mushroom> data, List<Object> objs, AttributeSelector selector) {
		Node node = new Node();
		if (data.stream().allMatch(mush -> mush.m_Class == Class_Label.edible)) {
			System.out.println("All identical, returning node...");
			node.label = Class_Label.edible;
			return node;
		}
		if (data.stream().allMatch(mush -> mush.m_Class == Class_Label.poisonous)) {
			System.out.println("All identical, returning node...");
			node.label = Class_Label.poisonous;
			return node;
		}
		
		if (objs.isEmpty()) {
			System.out.println("No more attributes");
			//majority vote
			node.label = data.stream().filter(mush -> mush.m_Class == Class_Label.edible).count() > data.size() / 2 ? Class_Label.edible : Class_Label.poisonous;
			return node;
		}
		Object att = selector.select(data, objs);
		List<Object> filteredAttributes = objs.stream().filter(objo -> objo != att).collect(Collectors.toList());
		for (Object obj : getAllPossibleValues(att)) {
			List<Mushroom> branchData = (List<Mushroom>) data.stream().filter(mush -> mush.getAttributeValue(att) == obj).collect(Collectors.toList());
			if (branchData.isEmpty()) {
				Node leafNode = new Node();
				leafNode.label = node.label = data.stream().filter(mush -> mush.m_Class == Class_Label.edible).count() > data.size() / 2 ? Class_Label.edible : Class_Label.poisonous;
				node.branches.add(new Branch<Object>(obj, leafNode));
			}
			else
				node.branches.add(new Branch<Object>(obj, buildTree(branchData, filteredAttributes, selector)));
		}
		return node;
			
	}
	
	public static Object[] getAllPossibleValues(Object obj) {

		if(obj.equals(Class_Label.class))
		{
			return Class_Label.values();
		}
		
		if(obj.equals(Cap_Shape.class))
		{
			return Cap_Shape.values();
		}
		if(obj.equals(Cap_Surface.class))
		{
			return Cap_Surface.values();
		}
		if(obj.equals(Cap_Color.class))
		{
			return Cap_Color.values();
		}
		if(obj.equals(Bruises.class))
		{
			return Bruises.values();
		}
		if(obj.equals(Odor.class))
		{
			return Odor.values();
		}
		if(obj.equals(Gill_Attachment.class))
		{
			return Gill_Attachment.values();
		}
		if(obj.equals(Gill_Spacing.class))
		{
			return Gill_Spacing.values();
		}
		if(obj.equals(Gill_Size.class))
		{
			return Gill_Size.values();
		}
		if(obj.equals(Gill_Color.class))
		{
			return Gill_Color.values();
		}
		if(obj.equals(Stalk_Shape.class))
		{
			return Stalk_Shape.values();
		}
		if(obj.equals(Stalk_Root.class))
		{
			return Stalk_Root.values();
		}
		if(obj.equals(Stalk_Surface_Above_Ring.class))
		{
			return Stalk_Surface_Above_Ring.values();
		}
		if(obj.equals(Stalk_Surface_Below_Ring.class))
		{
			return Stalk_Surface_Below_Ring.values();
		}
		if(obj.equals(Stalk_Color_Above_Ring.class))
		{
			return Stalk_Color_Above_Ring.values();
		}
		if(obj.equals(Stalk_Color_Below_Ring.class))
		{
			return Stalk_Color_Below_Ring.values();
		}
		if(obj.equals(Veil_Type.class))
		{
			return Veil_Type.values();
		}
		if(obj.equals(Veil_Color.class))
		{
			return Veil_Color.values();
		}
		if(obj.equals(Ring_Number.class))
		{
			return Ring_Number.values();
		}
		if(obj.equals(Ring_Type.class))
		{
			return Ring_Type.values();
		}
		if(obj.equals(Spore_Print_Color.class))
		{
			return Spore_Print_Color.values();
		}
		if(obj.equals(Population.class))
		{
			return Population.values();
		}
		if(obj.equals(Habitat.class))
		{
			return Habitat.values();
		}
		
		return null; //If we get down here something is wrong;
	}
}
