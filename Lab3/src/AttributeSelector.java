

import java.util.List;

public interface AttributeSelector {
	public Object select(List<Mushroom> data, List<Object> attributes);
}
