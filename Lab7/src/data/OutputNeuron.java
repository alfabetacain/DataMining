import java.util.*;

public class OutputNeuron{
	public double w1 = 0.25;
	public double w2 = 0.25;
	public double w0 = 0.25;


	public double activationFunction (double x1, double x2){

		double sum = x1*w1 + x2*w2 + 1*w0;

		return Math.tanh(sum);

	}
}