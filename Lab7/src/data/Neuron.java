import java.util.*;

public class Neuron {

	public double sepal_length_weight = 0.25;
	public double sepal_width_weight = 0.25;
	public double petal_length_weight = 0.25;
	public double petal_width_weight = 0.25;
	public double w0 = 0.25;
	public double activationFunction (float sepal_length, float sepal_width, float petal_length, float petal_width){

		double sum = sepal_length*sepal_length_weight + sepal_width*sepal_width_weight + petal_length*petal_length_weight + petal_width*petal_width_weight;


		return Math.tanh(sum);
	}


}