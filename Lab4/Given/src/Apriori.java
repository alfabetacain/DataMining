import java.util.*;



public class Apriori {
	/***
	 * The TRANSACTIONS 2-dimensional array holds the full data set for the lab
	 */
    static final int[][] TRANSACTIONS = new int[][] { { 1, 2, 3, 4, 5 }, { 1, 3, 5 }, { 2, 3, 5 }, { 1, 5 }, { 1, 3, 4 }, { 2, 3, 5 }, { 2, 3, 5 },
                    { 3, 4, 5 }, { 4, 5 }, { 2 }, { 2, 3 }, { 2, 3, 4 }, { 3, 4, 5 } };

    public static void main( String[] args ) {
        // TODO: Select a reasonable support threshold via trial-and-error. Can either be percentage or absolute value.
        final int supportThreshold = 4;
        apriori( TRANSACTIONS, supportThreshold );
    }

    public static List<ItemSet> apriori( int[][] transactions, int supportThreshold ) {

        Hashtable<ItemSet, Integer> frequentItemSets = generateFrequentItemSetsLevel1( transactions, supportThreshold );
        List<Hashtable<ItemSet,Integer>> complete = new ArrayList<>();
        complete.add(frequentItemSets);
        for (int k = 1; frequentItemSets.size() > 0; k++) 
        {
            System.out.println( "Finding frequent itemsets of length " + (k + 1) + "…" );

            //Added for Debug pupose
            System.out.printf("SupportThreshold: %s ; FrequentItemSets: %s.", supportThreshold, frequentItemSets);

            frequentItemSets = generateFrequentItemSets( supportThreshold, transactions, frequentItemSets );
            // TODO: add to list
            complete.add(frequentItemSets);
            System.out.println( " found " + frequentItemSets.size() );
        }

        //Added for Debug purpose
        System.out.printf("complete.size() : %d \n", complete.size());

        // TODO: create association rules from the frequent itemsets
        for (int i = complete.size()-1; i >= 0; i--)
        {
        	Hashtable<ItemSet,Integer> table = complete.get(i);

        	for(ItemSet set : table.keySet())
        	{
        		for ( int k = i-1; k >= 0 ; k--)
        		{
        			Hashtable<ItemSet,Integer> table2 = complete.get(k);
        			for( ItemSet set2 : table2.keySet()) 
        			{
        				if( contains(set2.set,set.set))
        				{
							double setSupp = table.get(set);
							double set2Supp = table2.get(set2);
							double implies = setSupp/set2Supp;
							System.out.println("Support -> " + set + "->" + setSupp);
							System.out.println("Support -> " + set2 + "->" + set2Supp);
        					System.out.println(set2 + "->" + set + "=" + implies);
        				}
        			}
        		}
        	}
        }


        // TODO: return something useful
        return null;
    }

    private static Hashtable<ItemSet, Integer> generateFrequentItemSets( int supportThreshold, int[][] transactions,
                    Hashtable<ItemSet, Integer> lowerLevelItemSets ) {

        // TODO: first generate candidate itemsets from the lower level itemsets - join step
		Hashtable<ItemSet,Integer> table = new Hashtable<>();
		int size = -1;

		for(ItemSet set: lowerLevelItemSets.keySet())
		{
			if (size == -1 )
				size = set.set.length;
			for(ItemSet set2 : lowerLevelItemSets.keySet())
			{
				if( set == set2) continue;
				ItemSet newset = joinSets(set,set2);
				if (newset != null)
					table.put(newset,0);
				//Added for Debug purpose
				System.out.printf("NewSet is: %s ; \n" , newset);
			}
		}

    	// TODO: then check if all subsets of the candidate item sets meet the apriori property - prune step    	
        Enumeration<ItemSet> iter = table.keys();

      	while(iter.hasMoreElements() )
      	{
      		ItemSet set = iter.nextElement();
      		if ( !prune(set, lowerLevelItemSets)) table.remove(set);
      	}

		/*
         * TODO: now check the support for the candidates left after pruning and add only those
         * that have enough support (i.e. support >= support_threshold) to the set
         */
		iter = table.keys();
		while (iter.hasMoreElements())
		{
			ItemSet set = iter.nextElement();
			int support = countSupport(set.set,transactions);
			if ( support > supportThreshold)
			{
				table.put(set,support);
			}
			else
			{
				table.remove(set);
			}
		}
        // TODO: return something useful
        return table;
    }
	
	private static boolean prune(ItemSet candidate, Hashtable<ItemSet,Integer> table)
	{
			int size = candidate.set.length-1;
			int start = 0 ;
      		int[] innerSet = candidate.set;
      		int[] subSet = new int[size];

      		while (start + size < innerSet.length)
      		{

      			System.arraycopy(innerSet, start++, subSet, 0, size);

      			ItemSet newset = new ItemSet(subSet);

      	        //Added for Debug purpose

      			//table.get(key) returns null if key not in hashtable added check to test if its in the table
      			if (!table.containsKey(newset))
      			{
					System.out.println("Pruning " + candidate + " because of " + newset + "...");
      				return false;
      			}

      			if ( table.get(newset) == 0)
      			{
      				return false;
      			}
      		}
      		return true;
	}

    private static ItemSet joinSets( ItemSet first, ItemSet second ) {
        // TODO: return something useful
		if(first.set.length != second.set.length) return null;
		
		int length = first.set.length;
		
		for(int i = 0; i < first.set.length-1; i++)
			if(first.set[i] != second.set[i]) return null;
		
		int[] newset = new int[first.set.length+1];
		System.arraycopy(first.set, 0, newset, 0, first.set.length-1);
		
		if( first.set[first.set.length-1] < second.set[second.set.length-1]) {
			newset[newset.length-2] = first.set[length-1];
			newset[newset.length-1] = second.set[length-1];
		}
		else
		{
			newset[newset.length-2] = second.set[length-1];
			newset[newset.length-1] = first.set[length-1];
		}
		return new ItemSet(newset);
    }

    private static Hashtable<ItemSet, Integer> generateFrequentItemSetsLevel1( int[][] transactions, int supportThreshold ) {
        // TODO: return something useful
		Hashtable<ItemSet,Integer> table = new Hashtable<>();
		
		for(int i = 0; i<transactions.length; i++)
		{
			for(int k = 0; k<transactions[i].length; k++)
			{
				ItemSet set = new ItemSet( new int[]{transactions[i][k]});
				if( table.containsKey(set)) table.put(set,table.get(set)+1);
				else table.put(set,1);
			}
		}

		Enumeration<ItemSet> enumerator = table.keys();
		while ( enumerator.hasMoreElements())
		{
			ItemSet set = enumerator.nextElement();
			if(table.get(set) < supportThreshold) table.remove(set);
		}
        return table;
    }

    private static int countSupport( int[] itemSet, int[][] transactions ) {
        // Assumes that items in ItemSets and transactions are both unique
		int count = 0;
		for (int i = 0; i < transactions.length; i++)
		{
			if( contains(itemSet, transactions[i])) count++;
		}
        // TODO: return something useful
        return count;
    }
	
	private static boolean contains(int[] containee, int[] container)
	{
		for (int i = 0; i < containee.length; i++)
		{
			boolean found = false;
			for(int k = 0; k < container.length; k++)
			{
				if(containee[i] == container[k]) found = true;
			}
			if(!found) return false;
		}
		return true;
	}

}
